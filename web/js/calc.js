$(document).ready(function(){
var input = document.getElementById('amount_NFT_to_buy');
var inputETH = document.getElementById('amount_ETH_to_buy');
var controls = document.querySelector('.box_qty_form_btn');
var cursVal =$('.val_in.val_eth').data('eth');

var coeficient = [1,08, 0.11, 0.13, 0.16, 0.20, 0.25, 0.31, 0.38, 0.47];
var currency = [' ETH', ' BTC', ' LTC', ' DASH', ' USD'];

$('.curs').html(cursVal);

$('.selectric').click(function(){
    $('.selectric_items').slideToggle();
    return false;
});

function mapEventToValue(event) {
    return event.target.value;
}
function compose() {
    var args = Array.prototype.slice.call(arguments);   
    return function(initValue) {
        return args.reduce(function(acc, el) {                
            return el(acc);
        }, initValue);
    };
}
function bindOutput(targetElement) {
    return function(newValue) {
        var curs = $('.val_in.selected');
        if(curs.hasClass('val_btc')){
        cursN = $('.val_in.val_btc').data('btc');
        } else if(curs.hasClass('val_ltc')){
            cursN = $('.val_in.val_ltc').data('ltc');
        } else if(curs.hasClass('val_eth')){
            cursN = $('.val_in.val_eth').data('eth');
        } else if(curs.hasClass('val_dash')){
            cursN = $('.val_in.val_dash').data('dash');
        } else if(curs.hasClass('inusd')){
            cursN = $('.val_in.val_eth').data('usd');
        } else if(curs.hasClass('val_bch')){
            cursN = $('.val_in.val_bch').data('bch');
        }
        
        var tElement = targetElement.value =(newValue*cursN/coeficient[0]).toFixed(4);
    };
}
function bind2Output(tElement, cursN) {
    return function(nValue) {  
        var curs = $('.val_in.selected');
        if(curs.hasClass('val_btc')){
        cursN = $('.val_in.val_btc').data('btc');
        } else if(curs.hasClass('val_ltc')){
            cursN = $('.val_in.val_ltc').data('ltc');
        } else if(curs.hasClass('val_eth')){
            cursN = $('.val_in.val_eth').data('eth');
        } else if(curs.hasClass('val_dash')){
            cursN = $('.val_in.val_dash').data('dash');
        } else if(curs.hasClass('inusd')){
            cursN = $('.val_in.val_eth').data('usd');
        } else if(curs.hasClass('val_bch')){
            cursN = $('.val_in.val_bch').data('bch');
        }
//        console.log(cursN);
        var btr = 0;
        var btr_b =0;
        if(nValue>20000000){
           inputETH.value = 20000000;
           nValue = 20000000;
        }
        btr_f = 0;
//        if(nValue < 500001){
            btr = nValue*coeficient[0];
//        }
//        if(nValue >=  500001 && nValue<900001){
//            btr_b = 500000*coeficient[0];
//            btr_f = (nValue - 500000)*coeficient[1];
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  900001 && nValue<1200001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1]);
//            btr_f = (nValue - 900000)*coeficient[2];            
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  1200001 && nValue<1450001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2]);
//            btr_f = (nValue - 1200000)*coeficient[3];            
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  1450001 && nValue<1650001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2])+(250000*coeficient[3]);
//            btr_f = (nValue - 1450000)*coeficient[4];            
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  1650001 && nValue<1800001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2])+(250000*coeficient[3])+(200000*coeficient[4]);
//            btr_f = (nValue - 1650000)*coeficient[5];            
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  1800001 && nValue<1900001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2])+(250000*coeficient[3])+(200000*coeficient[4])+(150000*coeficient[5]);
//            btr_f = (nValue - 1800000)*coeficient[6];            
//            btr = (+btr_f+btr_b);
//        }
//        if(nValue >=  1900001 && nValue<1950001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2])+(250000*coeficient[3])+(200000*coeficient[4])+(150000*coeficient[5])+(100000*coeficient[6]);
//            btr_f = (nValue - 1900000)*coeficient[7];            
//            btr = (+btr_f+btr_b);
//        }           
//        if(nValue >=  1950001){
//            btr_b = (500000*coeficient[0])+(400000*coeficient[1])+(300000*coeficient[2])+(250000*coeficient[3])+(200000*coeficient[4])+(150000*coeficient[5])+(100000*coeficient[6])+(50000*coeficient[7]);
//            btr_f = (nValue - 1950000)*coeficient[8];            
//            btr = (+btr_f+btr_b);
//        }
        
            btr = btr/cursN;
        var taElement = tElement.value =btr.toFixed(4);
    };
}
function getCallbacks(inputElement, outputCallback) {
    return {
        plus: function(event) {
            var num = !isNaN(+inputElement.value)&&(+inputElement.value<10000000);
            inputElement.value = num ? +inputElement.value + 1 : 1;
            outputCallback(input.value);

        },
        minus: function (event) {
            var num = !isNaN(+inputElement.value)&&(+inputElement.value>100);
            inputElement.value = num ? +inputElement.value - 1 : 1;
            outputCallback(input.value);
        }
    };
}
$('.val_in').click(function(){
    if(!$(this).hasClass('selected')){ 
        $('.val_in').removeClass('selected').removeClass('highlighted');
        $(this).addClass('selected').addClass('highlighted'); 
        var labeSelectric = document.querySelector('.selectric .label');
        var selectActive = document.querySelector('.selected').innerHTML;
        var selectActive2 = labeSelectric.innerHTML = selectActive;
    }
    var curs = $('.val_in.selected');
    var span_curs = $('.curs');
    var curs_currency = $('.curs_currency');
    if(curs.hasClass('val_btc')){
        cursVal = $('.val_in.val_btc').data('btc');
        curs_currency.text('BTC');
        span_curs.html($('.val_in.val_btc').data('btc'));
        $('.selectric_items').slideToggle();
    } else if(curs.hasClass('val_ltc')){
        cursVal = $('.val_in.val_ltc').data('ltc');
        span_curs.html($('.val_in.val_ltc').data('ltc'));        
        curs_currency.text('LTC');
        $('.selectric_items').slideToggle();
    } else if(curs.hasClass('val_eth')){
        cursVal = $('.val_in.val_eth').data('eth');
        span_curs.html($('.val_in.val_eth').data('eth'));
        curs_currency.text('ETHEREUM');
        $('.selectric_items').slideToggle();
    } else if(curs.hasClass('val_dash')){
        cursVal = $('.val_in.val_dash').data('dash');
        span_curs.html($('.val_in.val_dash').data('dash'));
        curs_currency.text('DASH');
        $('.selectric_items').slideToggle();
    } else if(curs.hasClass('inusd')){
        cursVal = $('.val_in.val_eth').data('usd');
        $('.selectric_items').slideToggle();
    } else if(curs.hasClass('val_bch')){
        cursVal = $('.val_in.val_bch').data('bch');
        span_curs.html($('.val_in.val_bch').data('bch'));
        curs_currency.text('BCH');
        $('.selectric_items').slideToggle();
    }
    var text;
    if(curs.hasClass('val_btc')){
        walletType = 'BTC';
        text = 'Bitcoin';
    } else if(curs.hasClass('val_ltc')){
        walletType = 'LTC';
        text = 'Litecoin ';
    } else if(curs.hasClass('val_eth')){
        walletType = 'ETHfrom'; 
        text = 'Ethereum';
    } else if(curs.hasClass('val_bch')){
        walletType = 'BCH';
        text = 'Bitcoin Cash';
    }
    $.post('get-wallet', {walletType:walletType}, function (response) {
        $('.currText').text(text);
        $('.currAb').text(walletType=='ETHfrom' ?'ETH':walletType );
        if (response.success) {
           $('#walletOther').val(response.success);
           $('#walletOther').prop("disabled", true);
           $("#walletBBtnApprove").removeClass('button_active');
            $("#walletBBtnEdit").addClass('button_active');
        } else {
            $('#walletOther').val('');
            $('#walletOther').prop("disabled", false);
            $("#walletBBtnApprove").addClass('button_active');
            $("#walletBBtnEdit").removeClass('button_active');
        }
    });
//    input.value = (Math.round(inputETH.value/cursVal * 10000) / 10000);
});

inputETH.addEventListener('input', compose(mapEventToValue, bind2Output(input)));
input.addEventListener('input', compose(mapEventToValue, bindOutput(inputETH)));
controls.addEventListener('click', function(event) {
    var callbacks = getCallbacks(input, bindOutput(inputETH, cursVal));
    event.preventDefault();
    var controlType = event.target.getAttribute('data-control');
    typeof callbacks[controlType] === 'function' && callbacks[controlType]();
});

});