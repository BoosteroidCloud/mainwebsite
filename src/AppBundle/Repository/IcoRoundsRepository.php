<?php

namespace AppBundle\Repository;

use AppBundle\Entity\IcoStages;
use AppBundle\Entity\IcoRounds;

/**
 * IcoRoundsRepository
 */
class IcoRoundsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $data
     * @return bool
     */
    public function addRound($data)
    {
        $em = $this->getEntityManager();
        $stage = $em->getRepository('AppBundle:IcoStages')->findOneByStage($data['stage']);

        $object = new IcoRounds();
        $object
                ->setRound($data['round'])
                ->setPrice($data['price'])
                ->setTokensAmount($data['tokensAmount'])
                ->setIcoStage($stage)
        ;

        $em->persist($object);
        $em->flush();

        return;
    }

    /**
     * @param integer $stage
     * @return IcoRounds
     */
    public function getFinalRoundInStage($stage)
    {
        $rounds = $this->getEntityManager()->getRepository('AppBundle:IcoRounds')->findBy(['icoStage' => $stage], ['price' => 'DESC']);

	return $rounds[0]; 
    }

    /**
     * @return array
     */
    public function getPricesFinalRounds()
    {
        $em = $this->getEntityManager();
        $stages = $em->getRepository('AppBundle:IcoStages')->findAll();

	$lastRounds = [];
	foreach($stages as $stage){
	    $lastRounds[] = $this->getFinalRoundInStage($stage)->getPrice();
	}

	return $lastRounds;
    }

    /**
     * @return array
     */
    public function getAllRounds()
    {
        $qb = $this->createQueryBuilder('icr');
        $query = $qb
                ->addOrderBy('icr.price', 'ASC')
                ->getQuery()
                ->getResult()
        ;
	return $query;
    }
}
