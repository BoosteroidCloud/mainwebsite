<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * Settings
 */
class Settings
{
    const LANDING_GROUP = 'landing';
    const SMART_CONTRACT_GROUP = 'smart_contract';

    const CURRENT_TOKEN_PRICE = 'current_token_price';
    const PRE_ICO_WALLET = 'pre_ico_wallet';
    const ICO_WALLET = 'ico_wallet';
    const INVESTED_AMOUNT = 'invested_amount';

    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $paramGroup;

    /**
     * @var string
     */
    private $paramKey;

    /**
     * @var string
     */
    private $value;

    /**
     * @var boolean
     */
    private $enabled;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paramGroup
     *
     * @param string $paramGroup
     *
     * @return Settings
     */
    public function setParamGroup($paramGroup)
    {
        $this->paramGroup = $paramGroup;

        return $this;
    }

    /**
     * Get paramGroup
     *
     * @return string
     */
    public function getParamGroup()
    {
        return $this->paramGroup;
    }

    /**
     * Set paramKey
     *
     * @param string $paramKey
     *
     * @return Settings
     */
    public function setParamKey($paramKey)
    {
        $this->paramKey = $paramKey;

        return $this;
    }

    /**
     * Get paramKey
     *
     * @return string
     */
    public function getParamKey()
    {
        return $this->paramKey;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return Settings
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Settings
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return boolean
     */
    public function isLandingGroup()
    {
        if($this->paramGroup === self::LANDING_GROUP){
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isSmartContractGroup()
    {
        if($this->paramGroup === self::SMART_CONTRACT_GROUP){
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isCurrentTokenPrice()
    {
        if($this->isSmartContractGroup() &&  $this->paramKey === self::CURRENT_TOKEN_PRICE){
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isPreIcoWallet()
    {
        if($this->isSmartContractGroup() &&  $this->paramKey === self::PRE_ICO_WALLET){
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isIcoWallet()
    {
        if($this->isSmartContractGroup() &&  $this->paramKey === self::ICO_WALLET){
            return true;
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function isInvestedAmount()
    {
        if($this->isLandingGroup() &&  $this->paramKey === self::INVESTED_AMOUNT){
            return true;
        }

        return false;
    }
}
