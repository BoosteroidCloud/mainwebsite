<?php

namespace AppBundle\Repository;

use AppBundle\Entity\IcoStages;

/**
 * IcoStagesRepository
 */
class IcoStagesRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $data
     * @return bool
     */
    public function addStage($data)
    {
        $em = $this->getEntityManager();

        $startDate = new \DateTime($data['startDate']);
        $endDate = new \DateTime($data['endDate']);

        $object = new IcoStages();
        $object
                ->setStage($data['stage'])
                ->setTotalTokens($data['totalTokens'])
                ->setStartDate($startDate)
                ->setEndDate($endDate)
        ;

        $em->persist($object);
        $em->flush();

        return;
    }

    /**
     * @return IcoStages
     */
    public function getCurrentStage()
    {
        $now = new \DateTime();

        $qb = $this->createQueryBuilder('ist');
        $query = $qb
                ->where(
                    $qb->expr()->andX(
                        $qb->expr()->lte('ist.startDate', ':date'),
                        $qb->expr()->gte('ist.endDate', ':date')
                    )
                )
                ->setParameter('date', $now)
                ->getQuery()
                ->getResult()
        ;
        return !empty($query) ? $query[0] : [];
    }

    /**
     * @return IcoStages
     */
    public function getNextStageForCurrentStage(IcoStages $stage)
    {
        $em = $this->getEntityManager();
        $stages = $em->getRepository($this->getEntityName())->findAll();
        $i = 0;
        $nextStage = '';

        foreach ($stages as $stage_) {
            if($stage_ == $stage){
                if(isset($stages[$i + 1])){
                    $nextStage = $stages[$i + 1];
                }else{
                    $nextStage = false;
                }
                break;
            }
        }

        return $nextStage;
    }

    /**
     * @return IcoStages
     */
    public function getNextStage()
    {
        $now = new \DateTime();

        $qb = $this->createQueryBuilder('ist');
        $query = $qb
                ->where(
                        $qb->expr()->gt('ist.endDate', ':date')
                )
                ->setParameter('date', $now)
                ->getQuery()
                ->getResult()
        ;
        return !empty($query) ? $query[0] : [];
    }
}
