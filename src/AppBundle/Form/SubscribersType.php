<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class SubscribersType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('email', EmailType::class, [
		    'required' => false,
		    'error_bubbling' => true,
                    'translation_domain' => 'landing',
		    'constraints' => [
			new Assert\Email(['checkHost' => true]),
		    ],
		]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
	$resolver->setDefaults(array(
	    'data_class' => 'AppBundle\Entity\Subscribers'
	));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
	return 'app_subscribe';
    }
}
