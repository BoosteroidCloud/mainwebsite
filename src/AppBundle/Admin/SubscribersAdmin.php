<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\Subscribers;

class SubscribersAdmin extends AbstractAdmin
{
    const LOCALES = [
        'EN' => 'en_EN',
        'RU' => 'ru_RU',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('email')
                ->add('locale')
                ->add('active')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('email')
                ->add('active', null, ['editable' => true])
                ->add('locale')
                ->add('confirmationToken')
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
		->with('Subscriber Data', ['class' => 'col-md-6'])
                    ->add('email')
                    ->add('locale', 'choice', [
                        'choices' => self::LOCALES,
                    ])
                    ->add('active')
                ->end()
		->with('Confirmation Token', ['class' => 'col-md-6'])
                    ->add('confirmationToken')
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Subscriber', ['class' => 'col-md-6'])
                    ->add('id')
                    ->add('email')
                    ->add('active')
                    ->add('locale')
                    ->add('confirmationToken')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param Subscribers $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Subscribers
            ? $object->getEmail()
            : 'New Subscriber';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('email')
		->assertNotBlank()
		->assertEmail()
            ->end()
            ->with('locale')
		->assertNotBlank()
		->assertChoice([
		    'choices' => self::LOCALES,
		])
            ->end()
            ->with('confirmationToken')
		->assertLength([
		    'min' => 32,
		])
        ;
    }
}
