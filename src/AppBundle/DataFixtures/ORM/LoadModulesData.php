<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadModulesData
 *
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LoadModulesData implements FixtureInterface
{
    /**
     * @var array
     */
    protected $data = [
	['module' => 'referral_system', 'enabled' => '1'],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
	foreach($this->data as $parameter){
	    $manager->getRepository('AppBundle:Modules')->addModule($parameter);
	}
    }
}