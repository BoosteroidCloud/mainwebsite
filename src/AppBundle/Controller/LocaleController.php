<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LocaleController extends Controller
{
    public function changeLanguageAction(Request $request)
    {
        $locale = $request->getLocale();
        $newLocal = $request->get('locale');
        if(!$newLocal){
            return new JsonResponse(['change'=> false ]);
        }
        if(!in_array($newLocal, ['ru_RU', 'en_EN', 'zh_CN', 'ja_JP'])){
            return new JsonResponse(['change'=> false ]);
        }
        if($locale == $newLocal){
            return new JsonResponse(['change'=> false ]);
        }

        $session = $request->getSession();
        $request->setLocale($newLocal);
        $session->set('_locale', $newLocal);

        return new JsonResponse(['change'=> true ]);
    }

}
