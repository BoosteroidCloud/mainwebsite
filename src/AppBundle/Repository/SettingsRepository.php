<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Settings;

/**
 * SettingsRepository
 */
class SettingsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $data
     * @return bool
     */
    public function addSetting($data)
    {
        $em = $this->getEntityManager();
        $obj = new Settings();
        $obj
                ->setParamGroup($data['paramGroup'])
                ->setParamKey($data['paramKey'])
                ->setValue($data['value'])
                ->setEnabled($data['enabled'])
        ;
        $em->persist($obj);
        $em->flush();

        return true;
    }

    /**
     * @return string
     */
    public function getInvestedAmount()
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
                ->select('s.value')
                ->where('s.paramGroup = :paramGroup')
                ->andWhere('s.paramKey = :paramKey')
                ->andWhere('s.enabled = 1')
                ->setParameters([
                    'paramGroup' => 'landing',
                    'paramKey' => 'invested_amount',
                ])
                ->getQuery()
                ->getSingleScalarResult()
        ;

        return $query;
    }

    /**
     * @return string
     */
    public function getIcoWellet()
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
                ->select('s.value')
                ->where('s.paramGroup = :paramGroup')
                ->andWhere('s.paramKey = :paramKey')
                ->andWhere('s.enabled = 1')
                ->setParameters([
                    'paramGroup' => 'smart_contract',
                    'paramKey' => 'ico_wallet',
                ])
                ->getQuery()
                ->getSingleScalarResult()
        ;

        return $query;
    }

    /**
     * @return Settings
     */
    public function getCurrentTokenPrice()
    {
        $qb = $this->createQueryBuilder('s');
        $query = $qb
                ->where('s.paramGroup = :paramGroup')
                ->andWhere('s.paramKey = :paramKey')
                ->setParameters([
                    'paramGroup' => 'smart_contract',
                    'paramKey' => 'current_token_price',
                ])
                ->getQuery()
                ->getOneOrNullResult()
        ;

        return $query;
    }
}
