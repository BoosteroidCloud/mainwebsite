<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadIcoStagesData
 *
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LoadIcoStagesData implements FixtureInterface
{
    /**
     * @var array
     */
    protected $data = [
	['stage' => '1-stage', 'totalTokens' => '100000000', 'startDate' => '23-10-2017', 'endDate' => '06-11-2017 23:59:59'],
	['stage' => '2-stage', 'totalTokens' => '300000000', 'startDate' => '27-11-2017', 'endDate' => '11-12-2017 23:59:59'],
	['stage' => '3-stage', 'totalTokens' => '400000000', 'startDate' => '15-01-2018', 'endDate' => '15-02-2018 23:59:59'],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
	foreach($this->data as $parameter){
	    $manager->getRepository('AppBundle:IcoStages')->addStage($parameter);
	}
    }
}