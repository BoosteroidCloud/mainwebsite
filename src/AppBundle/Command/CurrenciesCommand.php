<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class CurrenciesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
        		->setName('app:currencies:update')
        		->setDescription('Parse currencies rates');
        //
    }

    /**
     * Update crypto currencies rates and store it in DB
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.currencies_service')->setRatesCurrency();
    }
}
