<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use AppBundle\Entity\Users;
use EWZ\Bundle\RecaptchaBundle\Form\Type\EWZRecaptchaType;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class RegistrationType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('confirm', Type\CheckboxType::class, [
		    'required' => true,
		    'error_bubbling' => true,
		    'mapped' => false,
		    'constraints' => [
			new Assert\NotBlank(),
		    ]
		])
	;
        $builder->add('recaptcha', EWZRecaptchaType::class, array(
        
        'mapped'      => false,
        'constraints' => array(
            new RecaptchaTrue()
        )
    ));
	$builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
	    $user = $event->getForm()->getData();
	    $user->setRoles([Users::ROLE_OWNER]);
	    }
	);
    }


    public function getParent()
    {
	return 'FOS\UserBundle\Form\Type\RegistrationFormType';
    }

    public function getBlockPrefix()
    {
	return 'app_user_registration';
    }

}
