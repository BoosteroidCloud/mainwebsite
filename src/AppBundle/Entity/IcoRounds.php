<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * IcoStages
 */
class IcoRounds
{
    const FIRST_PRICE_AFTER_ICO = 4; // $4
    const SECOND_PRICE_AFTER_ICO = 6; // $6
    const STAGES_ROUNDS = [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
        6 => 6,
        7 => 7,
        8 => 8,
        9 => 9,
    ];

    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $round;

    /**
     * @var float
     */
    private $price;

    /**
     * @var integer
     */
    private $tokensAmount;

    /**
     * @var \AppBundle\Entity\IcoStages
     */
    private $icoStage;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set round
     *
     * @param integer $round
     *
     * @return IcoRounds
     */
    public function setRound($round)
    {
        $this->round = $round;

        return $this;
    }

    /**
     * Get round
     *
     * @return integer
     */
    public function getRound()
    {
        return $this->round;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return IcoRounds
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set tokensAmount
     *
     * @param integer $tokensAmount
     *
     * @return IcoRounds
     */
    public function setTokensAmount($tokensAmount)
    {
        $this->tokensAmount = $tokensAmount;

        return $this;
    }

    /**
     * Get tokensAmount
     *
     * @return integer
     */
    public function getTokensAmount()
    {
        return $this->tokensAmount;
    }

    /**
     * Set icoStage
     *
     * @param \AppBundle\Entity\IcoStages $icoStage
     *
     * @return IcoRounds
     */
    public function setIcoStage(\AppBundle\Entity\IcoStages $icoStage = null)
    {
        $this->icoStage = $icoStage;

        return $this;
    }

    /**
     * Get icoStage
     *
     * @return \AppBundle\Entity\IcoStages
     */
    public function getIcoStage()
    {
        return $this->icoStage;
    }

//    /**
//     * @return array
//     */
//    public static function getStagesAsLabels()
//    {
//        return [
//            'admin.ico_stages.stages_1' => self::ICO_STAGES_FIRST,
//            'admin.ico_stages.stages_2' => self::ICO_STAGES_SECOND,
//            'admin.ico_stages.stages_3' => self::ICO_STAGES_THIRD,
//        ];
//    }
}
