<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171022095820 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ico_rounds (id INT AUTO_INCREMENT NOT NULL, ico_stage_id INT DEFAULT NULL, round INT NOT NULL, price DOUBLE PRECISION NOT NULL, tokens_amount INT NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_FC7DA513CDA5BF6F (ico_stage_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE ico_stages (id INT AUTO_INCREMENT NOT NULL, stage VARCHAR(100) NOT NULL, total_tokens INT NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_E9A01A23C27C9369 (stage), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ico_rounds ADD CONSTRAINT FK_FC7DA513CDA5BF6F FOREIGN KEY (ico_stage_id) REFERENCES ico_stages (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE ico_rounds DROP FOREIGN KEY FK_FC7DA513CDA5BF6F');
        $this->addSql('DROP TABLE ico_rounds');
        $this->addSql('DROP TABLE ico_stages');
    }
}
