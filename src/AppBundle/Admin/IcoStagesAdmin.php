<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Entity\IcoStages;

class IcoStagesAdmin extends AbstractAdmin
{
    /**
     * @var array 
     */
    private $datepickerFilterSettings = [
        'format' => 'dd/MM/yyyy HH:mm:ss',
        'dp_side_by_side' => true,
        'dp_use_seconds' => false,
        'dp_show_today' => true,
    ];

    /**
     * @var array 
     */
    private $datepickerSettings = [
        'format' => 'dd/MM/yyyy HH:mm:ss',
        'dp_side_by_side' => true,
        'dp_show_today' => true,
    ];

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
		->remove('create')
		->remove('delete')
	;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
	$datagridMapper
		->add('stage')
		->add('totalTokens')
		->add('startDate', 'doctrine_orm_datetime_range', [
                    'field_type' => 'sonata_type_datetime_range_picker',
		    'field_options' => [
                        'field_options_start' => $this->datepickerFilterSettings,
                        'field_options_end' => $this->datepickerFilterSettings,
                    ]
		])
		->add('endDate', 'doctrine_orm_datetime_range', [
                    'field_type' => 'sonata_type_datetime_range_picker',
		    'field_options' => [
                        'field_options_start' => $this->datepickerFilterSettings,
                        'field_options_end' => $this->datepickerFilterSettings,
                    ]
		])
	;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
	$listMapper
		->add('id')
		->add('stage')
		->add('totalTokens')
		->add('startDate', null, ['format' => 'd/m/Y H:i:s'])
		->add('endDate', null, ['format' => 'd/m/Y H:i:s'])
		->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
		->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
		->add('_action', null, array(
		    'actions' => array(
			'show' => array(),
			'edit' => array(),
			'delete' => array(),
		    ),
		))
	;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	$object = $this->getSubject();
	$formMapper
                ->with('Ico Stage', ['class' => 'col-md-6'])
		    ->add('stage', null, [
			'attr' => [
			    'read_only' => true,
			],
		    ])
		    ->add('totalTokens')
		    ->add('startDate', 'sonata_type_datetime_picker', $this->datepickerSettings)
		    ->add('endDate', 'sonata_type_datetime_picker', $this->datepickerSettings)
		->end()
	;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
	$showMapper
                ->with('Ico Stage', ['class' => 'col-md-6'])
		    ->add('id')
		    ->add('stage')
		    ->add('totalTokens')
		    ->add('startDate', null, ['format' => 'd/m/Y H:i:s'])
		    ->add('endDate', null, ['format' => 'd/m/Y H:i:s'])
		    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
		    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
		->end()
	;
    }

    /**
     * @param Subscribers $object
     * @return string
     */
    public function toString($object)
    {
	return $object instanceof IcoStages ? $object->getStage() : 'New Ico Stage';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
	$errorElement
		->with('stage')
		    ->assertNotBlank()
		    ->assertChoice([
			'choices' => IcoStages::getStagesAsLabels(),
		    ])
		->end()
		    ->with('totalTokens')
		    ->assertNotBlank()
		    ->assertType([
			'type' => 'numeric',
		    ])
		->end()
		->with('startDate')
		    ->assertNotBlank()
		    ->assertDateTime()
		->end()
		->with('endDate')
		    ->assertNotBlank()
		    ->assertDateTime()
		->end()
	;
    }
}