$(function () {
    $('.sent_link').click(function (e) {
	e.preventDefault();
        $('.sent_link').prop("disabled", true);
	$.post(Routing.generate('admin_app_emailstemplates_sent_template'), {'id': $(this).data('template-id')}).done(
		function (data) {
		    location.reload();
		}
	);
    });
});