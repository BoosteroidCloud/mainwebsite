<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CoreController as Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Form\Admin\ReferalFiltersType;
use Symfony\Component\HttpFoundation\Request;

class ReferalReportAdminController extends Controller
{
    /**
     * @Route("/admin/app/bookings-reports", name="admin_referal_reports")
     */
    public function referalReportsAction(Request $request)
    {
	$filtersForm = $this->createForm(ReferalFiltersType::class);
        $usersReferals=[];
    if ($dataFilter = $request->request->get('app_referals_filters')) {
        $users = $this->getDoctrine()->getRepository('AppBundle:Users')->getUsersFromRangeCreated($dataFilter['start'], $dataFilter['end']);
        $usersReferals = $this->getDoctrine()->getRepository('AppBundle:Referrals')->getUserReferrals($users);
    }
	return $this->render('AppBundle:Admin:Reports/referal_reports.html.twig', [
	    'admin_pool' => $this->getAdminPool(),
	    'base_template' => $this->getBaseTemplate(),
	    'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
            'form' => $filtersForm->createView(),
            'usersReferals' =>  $usersReferals,
	]);
    }
    
    /**
     * @Route("/admin/app/remove-confirm-referals", name="admin_referal_confirm_remove")
     */
    public function referalConfirmRemoveAction(Request $request)
    {       
        if($array = $request->get('delete')){
            return $this->render('AppBundle:Admin:Reports/referal_delete.html.twig', [
                'admin_pool' => $this->getAdminPool(),
                'base_template' => $this->getBaseTemplate(),
                'blocks' => $this->container->getParameter('sonata.admin.configuration.dashboard_blocks'),
                'array' => $array,
          ]);  
        }
	return $this->redirectToRoute('admin_referal_reports');
    }
    
    /**
     * @Route("/admin/app/remove-referals", name="admin_referal_remove")
     */
    public function referalRemoveAction(Request $request)
    {	        
	$filtersForm = $this->createForm(ReferalFiltersType::class);
        $usersReferals=[];
        if($array = $request->get('delete')){
          $em = $this->getDoctrine()->getManager();
          
          foreach($array as $value){
                $user = $em->getRepository('AppBundle:Users')->find($value);
                $em->remove($user);                
                $refer = $em->getRepository('AppBundle:Referrals')->findOneBy(['refferalUserId'=>$value]);
                $em->remove($refer);
                $em->flush(); 
          }
          $this->addFlash('success', 'All selected referals are deleted!');        
        }
        return $this->redirectToRoute('admin_referal_reports');;
    }
}
