<?php

namespace AppBundle\Services\SmartContract;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DomCrawler\Crawler;
use AppBundle\Entity\CurrencyRates;
use AppBundle\Entity\CurrencyRatesHistory;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class CurrenciesService 
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
	$this->container = $container;
	$this->em = $em;
    }

    public function setRatesCurrency() 
    {
        $html = file_get_contents('https://bitinfocharts.com/ru/markets/');

        $crawler = new Crawler($html);
        $array_currency = [];
        $array_currency['BTC'] = $crawler->filter('#tr_1 td')->each(function (Crawler $node, $i) {
            return $node->attr('data-val');
        });
        $array_currency['ETH']= $crawler->filter('#tr_919 td')->each(function (Crawler $node, $i) {
            return $node->attr('data-val');
        });
        $array_currency['BCH'] = $crawler->filter('#tr_2087 td')->each(function (Crawler $node, $i) {
            return $node->attr('data-val');
        });
        $array_currency['DASH'] = $crawler->filter('#tr_119 td')->each(function (Crawler $node, $i) {
            return $node->attr('data-val');
        });
        $array_currency['LTC'] = $crawler->filter('#tr_2 td')->each(function (Crawler $node, $i) {
            return $node->attr('data-val');
        });

        foreach ($array_currency as $key => $value) {
            if ($currency = $this->em->getRepository('AppBundle:CurrencyRates')->findOneByCurrency($key)) {
                $currency->setRate(round($value[1], 2));
                $this->em->persist($currency);
            } else {
                $currency = new CurrencyRates();
                $currency
                        ->setRate(round($value[1], 2))
                        ->setCurrency($value[0])
                        ->setEnabled(true);
                $this->em->persist($currency);
            }
            $this->em->flush();

            $currencyHistory = new CurrencyRatesHistory();
            $currencyHistory
                    ->setRate(round($value[1], 2))
                    ->setCurrency($value[0])
                    ->setEnabled(true);
            $this->em->persist($currencyHistory);
            $this->em->flush();
        }

       return true;
    }

    public function getCurrentIcoStage()
    {
        //$stage = $this->em->getRepository('AppBundle:SmartContracts')->getStage();
        return;
    }
}
