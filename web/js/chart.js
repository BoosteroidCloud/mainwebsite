$(function(){
    var stage2end = $('#stage_2_end'),
        stage3end = $('#stage_3_end'),
        stageAfter1 = $('#after_ico_1'),
        stageAfter2 = $('#after_ico_2');

    $('#app_token_calculator').keyup(function(){
	var value = parseInt($(this).val()),
	    stage1 = $('#stage_1'),
	    stage2 = $('#stage_2'),
	    stage3 = $('#stage_3');
        if($(this).val().length <= 10){
             if(typeof(value) === "number" && !isNaN(value) && value !== 0){
                 stage1.text(calcCost(value, stage1.data('price')));
                 stage2.text(calcCost(value, stage2.data('price')));
                 stage3.text(calcCost(value, stage3.data('price')));
                 calcChart(value);
             }else if(isNaN(value)){
                 stage1.text(stage1.data('price') + '$');
                 stage2.text(stage2.data('price') + '$');
                 stage3.text(stage3.data('price') + '$');
                 calcChart(1);
             }
        }else{
            return false;
        }
    });

    function calcCost(value, stage){
	return (value * stage).toFixed(2) + '$';
    }

    function calcChart(value) {
	data = google.visualization.arrayToDataTable([
	    ['Element', 'Price, $', {role: 'style'}],
	    [lables[0], +(value * stage2end.data('price')).toFixed(2), '#b7efff'],
	    [lables[1], +(value * stage3end.data('price')).toFixed(2), '#b8dff8'],
	    [lables[2], +(value * stageAfter1.data('price')).toFixed(2), '#cbd1fb'],
	    [lables[3], +(value * stageAfter2.data('price')).toFixed(2), '#ccc1f4']
	]);

	chart.draw(data, options);
    }
});