$(function () {
    var selectedСurrency = $('.val_in.selected.highlighted');
    var inputUsd = $('#usd_amount');
    var inputTokens = $('#tokens_amount');

    $('.curs_currency').text(selectedСurrency.data('currency')),
    $('.curs').text(selectedСurrency.data('rate'));

//Open list
    $('.selectric').click(function () {
	$('.selectric_items').slideToggle();
	return false;
    });

//CHOOSE CURRENCY
    $('.val_in').click(function () {
	if (!$(this).hasClass('selected')) {
            var currencyName = $(this).data('currency');

	    $('.val_in').removeClass('selected highlighted');
	    $(this).addClass('selected highlighted');
            $('.selectric .label').text(currencyName);
            $('.curs').text($(this).data('rate'));
            $('.curs_currency').text(currencyName);

            var text;
            if(currencyName === 'BTC'){
                $('#modalContent').text("1B6RbVX2sFd8kLBab1aReUPKkfnha1Xzxs");
                walletType = currencyName;
                text = 'Bitcoin';
            } else if(currencyName === 'LTC'){
                $('#modalContent').text("LNrwaoyhSvWDVoPCnTG1riFmAHZC6jRwMr");
                walletType = currencyName;
                text = 'Litecoin ';
            } else if(currencyName === 'ETH'){
                walletType = 'ETHfrom';
                text = 'Ethereum';
                $('#modalContent').text("0x456095a509d6D7d93a252171dCF8920eA9E0Ea66");
            } else if(currencyName === 'BCH'){
                $('#modalContent').text("12BVUXL9tC98adaCjjEbMmHMdhkhGgrw3j");
                walletType = currencyName;
                text = 'Bitcoin Cash';
            }

            if (currencyName === 'ETH') {
                $('#step31').css('display', 'none');
                $('#step3').css('display', 'block');
            } else {
                $('#step3').css('display', 'none');
                $('#step31').css('display', 'block');
            }

            $.post(Routing.generate('get_wallet'), {walletType:walletType}, function (response) {
                $('.currText').text(text);
                $('.currAb').text( currencyName );
                if (response.success) {
                    $('#walletOther').val(response.success);
                    $('#walletOther').prop("disabled", true);
                    $("#walletBBtnApprove").removeClass('button_active');
                    $("#walletBBtnEdit").addClass('button_active');
                } else {
                    $('#walletOther').val('');
                    $('#walletOther').prop("disabled", false);
                    $("#walletBBtnApprove").addClass('button_active');
                    $("#walletBBtnEdit").removeClass('button_active');
                }
            });

            var btrValue = +inputTokens.val();
            if(typeof(btrValue) === "number" && !isNaN(btrValue) && btrValue !== 0){
                var cryptoRate = $(this).data('rate');
                $('#usd_amount').val(((btrValue * $('#tokens_amount').data('stage-rate') )/ cryptoRate).toFixed(5));
            }else if(isNaN(btrValue) || btrValue === 0){
                $('#usd_amount').val(0);
            }
	}
        $('.selectric_items').slideToggle();
    });

//BTR TOKENS AMOUNT
    inputTokens.keyup(function(){
        getUsdAmount();
    });
//USD AMOUNT
    inputUsd.keyup(function(){
        var amount  = +$(this).val();
        if(amount <= 0){
            inputTokens.val(calculateBtr(0));
        }else{
            inputTokens.val(calculateBtr(amount));
        }
    });

    function getUsdAmount(){
        var btrValue = +inputTokens.val();
        if(typeof(btrValue) === "number" && !isNaN(btrValue) && btrValue !== 0){
            inputUsd.val(calculateUsd(btrValue));
	}else if(isNaN(btrValue) || btrValue === 0){
            inputUsd.val(0);
        }
    }

    function calculateUsd(value){
        var cryptoRate = $('.val_in.selected.highlighted').data('rate');
        return ((value * $('#tokens_amount').data('stage-rate') )/ cryptoRate).toFixed(5);
    }

    function calculateBtr(value){
        var cryptoRate = $('.val_in.selected.highlighted').data('rate');
        return ((value * cryptoRate)/ $('#tokens_amount').data('stage-rate')).toFixed(5);
    }

    $('.box_qty_form_btn .qty_up.box_qty__plus').click(function(){
        var amount = +inputUsd.val() + 1;
        inputUsd.val(amount);
        inputTokens.val(calculateBtr(amount));
    });

    $('.box_qty_form_btn .qty_down.box_qty__minus').click(function(){
        var amount = +inputUsd.val() - 1;
        if(amount <= 0){
            inputUsd.val(1);
            inputTokens.val(calculateBtr(1));
        }else{
            inputUsd.val(amount);
            inputTokens.val(calculateBtr(amount));
        }
    });

//Update currency
    setInterval(function() {updateCurrencyRates();}, 900000); // 15 min

    function updateCurrencyRates(){
        $.post(Routing.generate('update_currency'), {'update': true}).done(function(data){
            if(data.success === true){
                $('.selectric_scroll ul li').each(function( index ){
                    var obj = JSON.parse(data.currencies[index]);
                    $('#tokens_amount').data('stage-rate', data.roundRate);
                    $(this).data('rate', obj.rate);
                    $(this).data('currency', obj.currency);
                });
            }else{
                return false;
            }
        });
    }
});