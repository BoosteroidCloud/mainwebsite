<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Subscribers;
use AppBundle\Entity\IcoRounds;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LandingService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
	$this->container = $container;
	$this->em = $em;
    }

    /**
     * @param Subscribers $subscriber
     * @param string $locale
     * @return boolean|Subscribers
     */
    public function setSubscriber(Subscribers $subscriber, $locale)
    {
        try{
            $subscriber
                    ->setActive(false)
                    ->setLocale($locale)
            ;

	    $this->em->persist($subscriber);
	    $this->em->flush();

            return $subscriber;
        } catch (Exception $e) {
	    return false;
	}
    }

    /**
     * @param Subscribers $subscriber
     * @return boolean|Subscribers
     */
    public function addConfirmationToken(Subscribers $subscriber)
    {
        try{
            $subscriber->setConfirmationToken($this->generateConfirmationToken($subscriber));
	    $this->em->persist($subscriber);
	    $this->em->flush();

            return $subscriber;
        } catch (Exception $e) {
	    return false;
	}
    }

    /**
     * @param Subscribers $subscriber
     * @param type $locale
     * @return boolean|Subscribers
     */
    public function generateConfirmationToken(Subscribers $subscriber)
    {
        $token = $this->encode($subscriber->getCreatedAt()->format('d-m-Y h:i:s'));

        return $token;
    }

    /**
     * @param Subscribers $subscriber
     * @return type
     */
    public function activateSubscriber(Subscribers $subscriber, $token2)
    {
        if($this->compareTokens($subscriber->getConfirmationToken(), $token2)){
            $subscriber->setActive(true);

	    $this->em->persist($subscriber);
	    $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * @param string $data
     * @return string
     * @throws \LogicException
     */
    public function encode($data)
    {
        if (0 === strlen($data)) {
            return;
        }

	return md5($data);
    }

    /** 
     * @return bool true if the two tokens are the same, false otherwise
     */
    public function compareTokens($token1, $token2)
    {
        return hash_equals($token1, $token2);
    }
    
    /**
     * Get data for price growth chart
     * 
     * @return array
     */
    public function getChartData()
    {
        $data = $this->em->getRepository('AppBundle:IcoRounds')->getPricesFinalRounds();
        $data[] = IcoRounds::FIRST_PRICE_AFTER_ICO;
        $data[] = IcoRounds::SECOND_PRICE_AFTER_ICO;
        
        return $data;
    }
}
