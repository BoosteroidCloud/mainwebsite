<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace AppBundle\Services;

use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Mailer\MailerInterface;
use Mailgun\Mailgun;

class MailerFosUserService implements MailerInterface
{
    /**
     * @var ezcMailTransport
     */
    /**
     * @var ContainerInterface
     */
    private $container;

    /*
     * mailgun client
     * $mgClient
     */
    private $mgClient;
    
    protected $router;
    
    public function __construct(ContainerInterface $container,RouterInterface $router)
    {
        $this->router = $router;
        $this->container = $container;
        $this->mgClient = new Mailgun('key-3471fd8b321afec148c4af47b40c9516');
    }
    public function sendConfirmationEmailMessage(UserInterface $user)
    {     
        $translator = $this->container->get('translator.default');
        $subject = $translator->trans('registration.subject', [], 'mail');
        $url = $this->router->generate('fos_user_registration_confirm', array('token' => $user->getConfirmationToken()), true);
        $rendered = $this->getTemplating()->render('AppBundle:Mail:confrim_registration.html.twig', array(
            'user' => $user,
            'confirmationUrl' =>  $url
        ));
        
        $this->sendEmailMessage($rendered, $user->getEmail(), $subject);
    }
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $translator = $this->container->get('translator.default');
        $subject = $translator->trans('registration.subject', [], 'mail');
        $url = $this->router->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $rendered = $this->getTemplating()->render('AppBundle:Security:Resetting/email.html.twig', array(
            'user' => $user,
            'confirmationUrl' => $url
        ));
        $this->sendEmailMessage($rendered, $user->getEmail(), $subject);
    }
    
    public function sendEmailMessage($rendered, $email, $subject){
        
        $mgClient = $this->mgClient;
        $domain = "blog.boosteroid.com";
        $messageBuilder = $mgClient->MessageBuilder();
        $messageBuilder->setFromAddress($this->getMailerFrom());
        $messageBuilder->addToRecipient($email);
        $messageBuilder->setSubject($subject);
        $messageBuilder->setHtmlBody($rendered);
        $res = $mgClient->post("{$domain}/messages", $messageBuilder->getMessage());
        if($res->http_response_code=='200'){
          return  true;
        }
        return false;
    }
    
    /**
     * @return string
     */
    protected function getMailerFrom()
    {
        return iconv("UTF-8", "ISO-8859-1//IGNORE", $this->container->getParameter('mail_from'));
    }
    
    /**
     * @return TwigService
     */
    protected function getTemplating()
    {
        return $this->container->get('templating');
    }
}