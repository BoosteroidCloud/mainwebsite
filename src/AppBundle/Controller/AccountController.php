<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\BountyProgramType;
use AppBundle\Entity\BountyProgram;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Form\ReferralLinkType;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\UserType;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class AccountController extends Controller
{
    /**
     * @Route("/profile", name="profile")
     */
    public function profileAction(Request $request)
    {
        $user = $this->getUser();
        if(!$user){
	    return $this->redirectToRoute('fos_user_security_login');
	}

        $errors = [];
        $translator = $this->get('translator.default');
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(UserType::class, $this->getUser());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
	    $userInfo = $form->getData();
	    $em->persist($userInfo);
	    $em->flush();

            $this->addFlash('success', $translator->trans('form.profile.update', [], 'account'));
            return $this->redirectToRoute('profile');
        }

        /** @var $dispatcher EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        /** @var $formFactory FactoryInterface */
        $formFactory = $this->get('fos_user.change_password.form.factory');

        $formPass = $formFactory->createForm();
        $formPass->setData($user);

        $formPass->handleRequest($request);

        if ($formPass->isSubmitted() && $formPass->isValid()) {
            /** @var $userManager UserManagerInterface */
            $userManager = $this->get('fos_user.user_manager');

            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_SUCCESS, $event);

            $userManager->updateUser($user);

            if (null === $response = $event->getResponse()) {
                $url = $this->generateUrl('profile');
                $response = new RedirectResponse($url);
            }

            $dispatcher->dispatch(FOSUserEvents::CHANGE_PASSWORD_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
            $this->addFlash('success', $translator->trans('form.profile.password', [], 'account'));
            return $response;
        }

        if($form->getErrors()){
	    $errors = $form->getErrors();
	}

        return $this->render('AppBundle:Account:profile.html.twig',[
                    'formPass' => $formPass->createView(),
                    'form' =>$form->createView(),
                    'errors' => $errors,
                ]);
    }

    /**
     * @Route("/bounty", name="bounty")
     */
    public function bountyAction(Request $request)
    {
	if(!$this->getUser()){
	    return $this->redirectToRoute('fos_user_security_login');
	}

        return $this->redirectToRoute('token_sale');

//	$errors = [];
//	$em = $this->getDoctrine()->getManager();

        /**
         * Bounty programm
         */
//	$form = $this->createForm(BountyProgramType::class, new BountyProgram());
//	$form->handleRequest($request);
//
//	if($form->isSubmitted() && $form->isValid()){
//	    $bounty = $form->getData();
//	    $bounty->setUser($this->getUser());
//	    $em->persist($bounty);
//	    $em->flush();
//
//	    $this->addFlash('success', $translator->trans('form.bounty.success', [], 'account'));
//	    return $this->redirectToRoute('bounty');
//	}
//
//	if($form->getErrors()->getChildren()){
//	    $errors = $form->getErrors();
//	}

        /**
         * Referral links module
         */
//        $referralForm = $this->createForm(ReferralLinkType::class, ['userId' => $this->getUser()->getId()]);
//        $referralForm->handleRequest($request);
//
//	if($referralForm->isSubmitted() && $referralForm->isValid()){
//            $em = $this->getDoctrine()->getManager();
//	    $referral = $referralForm->getData();
//            $user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($referral['userId']);
//
//            if(!empty($user)){
//                $token = $this->get('app.referral_service')->generateReferralLink($user);
//                $user->setReferralToken($token);
//                $em->persist($user);
//                $em->flush();
//
//                return $this->redirectToRoute('bounty');
//            }
//	}
//
//       $totalReferals = $this->getDoctrine()->getRepository('AppBundle:Referrals')->getActiveReferrals($this->getUser());
//
//        return $this->render('AppBundle:Account:bounty.html.twig', [
//	    //'form' => $form->createView(),
//	    'errors' => $errors,
//            'referralForm' => $referralForm->createView(),
//            'totalReferals' => $totalReferals,
//            'referralSystem' => $this->getDoctrine()->getRepository('AppBundle:Modules')->getModuleStatus('referral_system'),
//	]);
    }

    /**
     * @Route("/bounty/referral-link", name="referral_link", options={"expose"=true})
     */
    public function createReferralLink(Request $request)
    {
        $form = $this->createForm(ReferralLinkType::class, ['userId' => $this->getUser()->getId()]);
        $form->handleRequest($request);

	if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
	    $referral = $form->getData();
            $user = $this->getDoctrine()->getRepository('AppBundle:Users')->find($referral['userId']);

            if(!empty($user)){
                $token = $this->get('app.referral_service')->generateReferalLink($user);
                $user->setReferralToken($token);
                $em->persist($user);
                $em->flush();

                return new JsonResponse(['success' => true, 'token' => $token]);
            }
	}
        return new JsonResponse(['success' => false]);
    }
}
