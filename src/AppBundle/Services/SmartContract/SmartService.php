<?php

namespace AppBundle\Services\SmartContract;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class SmartService 
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
	$this->container = $container;
	$this->em = $em;
    }
 
    public function getRemainToken() {
        $smartData = file_get_contents('https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0xC01281a1F3f202AfFae40E4DF130E4CC8ED108cB&address=0xbD00cE9C7c88E05373ecabA02a63277e8d9cF3e5&tag=latest&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS');
	$result = json_decode($smartData);
        if($result->status==1) {
            return $result;
        }
        return 0;        
    }
    
    public function getSaleTokens() {
//        $smartData = file_get_contents('https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress=0x0C0Ed9A68C9cA0eCed8612E2f80a519867F36869&address=0xA82905520C8bE0Ed0C3fb3d738D290dd21E19392&tag=latest&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS');
	$smartData = file_get_contents('https://api.etherscan.io/api?module=account&action=balance&address=0xfFF58E2Bd882dd1D22f8EEBbA2c5B3d7f07A7fe9&tag=latest&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS');
	$result = json_decode($smartData);
        $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->find(1);
        $remainToken = $this->getRemainToken();
        $result = ($smartContract->getTotalTokens() - $remainToken->result); 

        return $result;
    }
    public function getEthUsd() {
        $smartData = file_get_contents('https://api.etherscan.io/api?module=stats&action=ethprice&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS');
	$result = json_decode($smartData);
        if($result->status==1) {
            return $result->result->ethusd;
        }
        return 0;
    }

    public function setRamainTokens() {
        $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->find(1);
        $smartContract->setBalance($this->getRemainToken()->result);
        $this->em->persist($smartContract);
        return $this->em->flush() ? true : false;
    }
}