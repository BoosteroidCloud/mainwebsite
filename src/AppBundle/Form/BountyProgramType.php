<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class BountyProgramType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('link', UrlType::class, [
		    'trim' => true,
		    'attr' => ['placeholder' => 'Enter your link'],
		    'error_bubbling' => true,
		    'constraints' => [
			new Assert\NotBlank(),
			new Assert\Length([
			    'min' => 6,
			]),
			new Assert\Url([
			    'checkDNS' => true,
			]),
		    ]
		])
	;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
	$resolver->setDefaults(array(
	    'data_class' => 'AppBundle\Entity\BountyProgram'
	));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
	return 'app_bounty';
    }
}
