<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Modules;

/**
 * ModulesRepository
 */
class ModulesRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param array $data
     * @return bool
     */
    public function addModule($data)
    {
        $em = $this->getEntityManager();
        $obj = new Modules();
        $obj
                ->setModule($data['module'])
                ->setEnabled($data['enabled'])
        ;
        $em->persist($obj);
        $em->flush();

        return true;
    }

    /**
     * @param string $module
     * @return bool
     */
    public function getModuleStatus($module)
    {
        $qb = $this->createQueryBuilder('m');
        $query = $qb
                ->select('m.enabled')
                ->where('m.module = :module')
                ->setParameter('module', $module)
                ->getQuery()
                ->getSingleScalarResult()
        ;

        return (bool)$query;
    }
}
