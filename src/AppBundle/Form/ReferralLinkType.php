<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Core\Type;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class ReferralLinkType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('userId', Type\HiddenType::class, [
		    'required' => true,
		    'error_bubbling' => true,
		    'mapped' => false,
		    'constraints' => [
			new Assert\NotBlank(),
                        new Assert\Type([
                            'type' => 'numeric',
                        ]),
		    ],
		])
	;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
	return 'app_referral';
    }
}
	