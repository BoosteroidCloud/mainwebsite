<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\SmartContracts;

/**
 * LoadModulesData
 *
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LoadSmartContractData implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $object = $this->em->getRepository('AppBundle:SmartContracts')->createEmptyContract();
        $manager->persist($object);
        $manager->flush();
    }
}