<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\Users;

class UsersAdmin extends AbstractAdmin
{
    public function prePersist($obj)
    {
        $this->updateUser($obj);
    }

    public function preUpdate($obj)
    {
        $this->updateUser($obj);
    }

    public function updateUser($obj)
    {
	$um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');

        if ($obj->getPlainPassword()) {
            $obj->setPlainPassword($obj->getPlainPassword());
	    $um->updateUser($obj);
        }
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
	$datagridMapper
		->add('firstName')
		->add('lastName')
		->add('username')
		->add('email')
		->add('enabled')
		->add('roles')
		->add('phone')
                ->add('walletETH')
                ->add('walletBTC')
                ->add('walletBCH')
                ->add('walletLTC')
                ->add('walletETHfrom')
                ->add('walletOther')
                ->add('amountBTR')
		->add('createdAt', 'doctrine_orm_date_range')
		->add('updatedAt', 'doctrine_orm_date_range')
	;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
	$listMapper
		->add('id')
		->add('firstName')
		->add('lastName')
//		->add('username')
		->add('email')
		->add('phone')
//		->add('roles')
                ->add('walletETH')
                ->add('walletBTC')
                ->add('walletBCH')
                ->add('walletLTC')
                ->add('walletETHfrom')
                ->add('walletOther')
                ->add('amountBTR')
		->add('enabled', null, ['ediable' => true])
		->add('lastLogin', null, ['format' => 'd/m/Y H:i'])
		->add('createdAt', null, ['format' => 'd/m/Y H:i'])
		->add('updatedAt', null, ['format' => 'd/m/Y H:i'])
		->add('_action', null, array(
		    'actions' => array(
			'show' => array(),
			'edit' => array(),
			'delete' => array(),
		    ),
		))
	;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	$obj = $this->getSubject();

	$paswordFieldOptions = [
	    'type' => 'password',
	    'options' => ['translation_domain' => 'FOSUserBundle'],
	    'first_options' => ['label' => 'form.password'],
	    'second_options' => ['label' => 'form.password_confirmation'],
	    'invalid_message' => 'fos_user.password.mismatch',
	    'required' => false,
	];

	if (!$obj->getId()){
	    $paswordFieldOptions['required'] = true;
	    $paswordTab = 'Password';
	}else{
	    $paswordTab = 'Change Password';
	}

	$formMapper
		->tab('User Profile')
		    ->with('Profile', ['class' => 'col-md-8'])
			->add('firstName')
			->add('lastName')
			->add('email', 'email')
			->add('phone')
//			->add('username')
                        ->add('walletETH')
                        ->add('walletBTC')
                        ->add('walletBCH')
                        ->add('walletLTC')
                        ->add('walletETHfrom')
                        ->add('walletOther')
                        ->add('amountBTR')
		    ->end()
		    ->with('Settings', ['class' => 'col-md-4'])
			->add('roles', 'choice', [
			    'label' => 'Role',
			    'choices' => $obj->getRolesAsLabels(),
			    'multiple' => true,
			])
			->add('enabled')
		    ->end()
		->end()
		->tab('Security')
		    ->with($paswordTab, ['class' => 'col-md-4'])
			->add('plainPassword', 'repeated', $paswordFieldOptions)
		    ->end()
		->end()
	;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
	$showMapper
		->add('id')
		->add('firstName')
		->add('lastName')
//		->add('username')
		->add('email')
		->add('phone')
		->add('enabled')
		->add('roles')
                ->add('walletETH')
                ->add('walletBTC')
                ->add('walletBCH')
                ->add('walletLTC')
                ->add('walletETHfrom')
                ->add('walletOther')
                ->add('amountBTR')
		->add('lastLogin', null, ['format' => 'd/m/Y H:i'])
		->add('createdAt', null, ['format' => 'd/m/Y H:i'])
		->add('updatedAt', null, ['format' => 'd/m/Y H:i'])
	;
    }

    /**
     * @param Users $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Users
            ? $object->getFullName()
            : 'New User';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
//            ->with('firstName')
//		->assertNotBlank()
//		->assertLength([
//		    'min' => 3,
//		])
//            ->end()
//            ->with('lastName')
//		->assertNotBlank()
//		->assertLength([
//		    'min' => 3,
//		])
//            ->end()
//            ->with('username')
//		->assertNotBlank()
//		->assertLength([
//		    'min' => 3,
//		])
//            ->end()
            ->with('email')
		->assertNotBlank()
		->assertEmail()
            ->end()
//            ->with('phone')
//		->assertRegex([
//		    'pattern' => "/^[\d-\s\+\(\)]+$/",
//		])
//		->assertLength([
//		    'min' => 10,
//		])
//            ->end()
//            ->with('plainPassword')
//		->assertLength([
//		    'min' => 3,
//		])
//            ->end()
        ;
    }
}
