<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadIcoRoundsData
 *
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LoadIcoRoundsData implements FixtureInterface
{
    /**
     * @var array
     */
    protected $data = [
	['stage' => '1-stage', 'round' => '1', 'price' => '0.48', 'tokensAmount' => '50000000'],
	['stage' => '1-stage', 'round' => '2', 'price' => '0.58', 'tokensAmount' => '50000000'],
	['stage' => '2-stage', 'round' => '1', 'price' => '1.08', 'tokensAmount' => '60000000'],
	['stage' => '2-stage', 'round' => '2', 'price' => '1.18', 'tokensAmount' => '70000000'],
	['stage' => '2-stage', 'round' => '3', 'price' => '1.28', 'tokensAmount' => '80000000'],
	['stage' => '2-stage', 'round' => '4', 'price' => '1.38', 'tokensAmount' => '90000000'],
	['stage' => '3-stage', 'round' => '1', 'price' => '1.88', 'tokensAmount' => '100000000'],
	['stage' => '3-stage', 'round' => '2', 'price' => '2.18', 'tokensAmount' => '100000000'],
	['stage' => '3-stage', 'round' => '3', 'price' => '2.48', 'tokensAmount' => '100000000'],
	['stage' => '3-stage', 'round' => '4', 'price' => '2.78', 'tokensAmount' => '100000000'],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
	foreach($this->data as $parameter){
	    $manager->getRepository('AppBundle:IcoRounds')->addRound($parameter);
	}
    }
}