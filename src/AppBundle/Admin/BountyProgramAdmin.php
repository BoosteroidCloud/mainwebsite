<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\BountyProgram;

class BountyProgramAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'user.email',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
	$datagridMapper
		->add('user', 'doctrine_orm_model_autocomplete', ['label' => 'User full name'], null, ['property' => 'email'])
		->add('link')
		->add('bonusTokens')
	;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
	$listMapper
		->add('id')
		->add('user.email')
		->add('link', 'url', [
		    'attributes' => [
			'rel' => 'noreferrer nofollow noopener',
			'target' => '_blank',
		    ],
		])
		->add('bonusTokens')
		->add('createdAt', null, ['format' => 'd/m/Y H:i'])
		->add('_action', null, array(
		    'actions' => array(
			'show' => array(),
			'edit' => array(),
			'delete' => array(),
		    ),
		))
	;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	$formMapper
		->with('User Bonus', ['class' => 'col-md-5'])
		    ->add('user', 'sonata_type_model_list', array(
			'btn_add' => false,
		    ), array(
			'placeholder' => 'Select user'
		    ))
		    ->add('bonusTokens', null, ['label' => 'Bonus Token'])
		->end()
		->with('Link', ['class' => 'col-md-7'])
		    ->add('link', 'url',[
			'trim' => true,
			'attr' => ['placeholder' => 'Enter your link'],
		    ])
		->end()
	;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
	$showMapper
		->add('id')
		->add('user.email')
		->add('link', 'url', [
		    'attributes' => [
			'rel' => 'noreferrer nofollow noopener',
			'target' => '_blank',
		    ],
		])
		->add('bonusTokens')
		->add('createdAt', null, ['format' => 'd/m/Y H:i'])
		->add('updatedAt', null, ['format' => 'd/m/Y H:i'])
	;
    }

    /**
     * @param BountyProgram $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof BountyProgram
            ? 'Bounty '.$object->getId()
            : 'New Bounty';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('user')
		->assertNotBlank()
	    ->end()
            ->with('link')
		->assertNotBlank()
		->assertUrl([
		    'checkDNS' => true,
		])
	    ->end()
            ->with('bonusTokens')
		->assertType([
		    'type' => 'integer',
		])
	    ->end()
        ;
    }
}
