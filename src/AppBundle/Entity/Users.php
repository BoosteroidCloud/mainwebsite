<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\Timestampable;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Users
 * 
 * @UniqueEntity(fields={"email"})
 * @UniqueEntity(fields={"username"})
 */
class Users extends BaseUser
{
    const ROLE_OWNER = 'ROLE_OWNER';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const REFERRAL_RATIO = 5; //BTR

    use Timestampable;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var string
     */
    private $phone;

    /**
     * @var string
     */
    private $referralToken;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $bounties;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $referrals;

    public function __construct()
    {
        parent::__construct();
        $this->bounties = new \Doctrine\Common\Collections\ArrayCollection();
        $this->referrals = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Users
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set referralToken
     *
     * @param string $referralToken
     *
     * @return Users
     */
    public function setReferralToken($referralToken)
    {
        $this->referralToken = $referralToken;

        return $this;
    }

    /**
     * Get referralToken
     *
     * @return string
     */
    public function getReferralToken()
    {
        return $this->referralToken;
    }

    /**
     * Add bounty
     *
     * @param \AppBundle\Entity\BountyProgram $bounty
     *
     * @return Users
     */
    public function addBounty(\AppBundle\Entity\BountyProgram $bounty)
    {
        $this->bounties[] = $bounty;

        return $this;
    }

    /**
     * Remove bounty
     *
     * @param \AppBundle\Entity\BountyProgram $bounty
     */
    public function removeBounty(\AppBundle\Entity\BountyProgram $bounty)
    {
        $this->bounties->removeElement($bounty);
    }

    /**
     * Get bounties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBounties()
    {
        return $this->bounties;
    }

    /**
     * Add referral
     *
     * @param \AppBundle\Entity\Referrals $referral
     *
     * @return Users
     */
    public function addReferral(\AppBundle\Entity\Referrals $referral)
    {
        $this->referrals[] = $referral;

        return $this;
    }

    /**
     * Remove referral
     *
     * @param \AppBundle\Entity\Referrals $referral
     */
    public function removeReferral(\AppBundle\Entity\Referrals $referral)
    {
        $this->referrals->removeElement($referral);
    }

    /**
     * Get referrals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReferrals()
    {
        return $this->referrals;
    }

    /**
     * Get user full name
     * 
     * @return string
     */
    public function getFullName()
    {
	return $this->firstName . ' ' . $this->lastName;
    }

    /**
     * {@inheritdoc}
     */
    public function isOwner()
    {
        return $this->hasRole(self::ROLE_OWNER);
    }

    /**
     * @return array
     */
    public function getRolesAsLabels()
    {
	return [
	    self::ROLE_OWNER => self::ROLE_OWNER,
	    self::ROLE_ADMIN => self::ROLE_ADMIN,
	    self::ROLE_SUPER_ADMIN => self::ROLE_SUPER_ADMIN,
	];
    }

    /**
     * @var string
     */
    private $walletETH;


    /**
     * Set walletETH
     *
     * @param string $walletETH
     *
     * @return Users
     */
    public function setWalletETH($walletETH)
    {
        $this->walletETH = $walletETH;

        return $this;
    }

    /**
     * Get walletETH
     *
     * @return string
     */
    public function getWalletETH()
    {
        return $this->walletETH;
    }
    /**
     * @var string
     */
    private $amountBTR;


    /**
     * Set amountBTR
     *
     * @param string $amountBTR
     *
     * @return Users
     */
    public function setAmountBTR($amountBTR)
    {
        $this->amountBTR = $amountBTR;

        return $this;
    }

    /**
     * Get amountBTR
     *
     * @return string
     */
    public function getAmountBTR()
    {
        return $this->amountBTR;
    }

    /**
     * @param integer $totalActiveUsers
     * @return integer
     */
    public function getTotalReferralTokens($totalActiveUsers)
    {
        return $totalActiveUsers * self::REFERRAL_RATIO;
    }

    /**
     * @return int
     */
    public function getTotalRegistrations()
    {
        return (int) $this->getReferrals()->count();
    }
    
    /**
     * @var string
     */
    private $walletBTC;

    /**
     * @var string
     */
    private $walletBCH;

    /**
     * @var string
     */
    private $walletLTC;


    /**
     * Set walletBTC
     *
     * @param string $walletBTC
     *
     * @return Users
     */
    public function setWalletBTC($walletBTC)
    {
        $this->walletBTC = $walletBTC;

        return $this;
    }

    /**
     * Get walletBTC
     *
     * @return string
     */
    public function getWalletBTC()
    {
        return $this->walletBTC;
    }

    /**
     * Set walletBCH
     *
     * @param string $walletBCH
     *
     * @return Users
     */
    public function setWalletBCH($walletBCH)
    {
        $this->walletBCH = $walletBCH;

        return $this;
    }

    /**
     * Get walletBCH
     *
     * @return string
     */
    public function getWalletBCH()
    {
        return $this->walletBCH;
    }

    /**
     * Set walletLTC
     *
     * @param string $walletLTC
     *
     * @return Users
     */
    public function setWalletLTC($walletLTC)
    {
        $this->walletLTC = $walletLTC;

        return $this;
    }

    /**
     * Get walletLTC
     *
     * @return string
     */
    public function getWalletLTC()
    {
        return $this->walletLTC;
    }
    /**
     * @var string
     */
    private $walletETHfrom;

    /**
     * @var string
     */
    private $walletOther;


    /**
     * Set walletETHfrom
     *
     * @param string $walletETHfrom
     *
     * @return Users
     */
    public function setWalletETHfrom($walletETHfrom)
    {
        $this->walletETHfrom = $walletETHfrom;

        return $this;
    }

    /**
     * Get walletETHfrom
     *
     * @return string
     */
    public function getWalletETHfrom()
    {
        return $this->walletETHfrom;
    }

    /**
     * Set walletOther
     *
     * @param string $walletOther
     *
     * @return Users
     */
    public function setWalletOther($walletOther)
    {
        $this->walletOther = $walletOther;

        return $this;
    }

    /**
     * Get walletOther
     *
     * @return string
     */
    public function getWalletOther()
    {
        return $this->walletOther;
    }
}
