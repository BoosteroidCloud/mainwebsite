<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * SmartContracts
 */
class SmartContracts
{
    const NOT_ICO_TOTAL_AMOUNT = 200000000;
    const STAGE_PRE_ICO = 'pre-ICO';
    const STAGE_ICO = 'ICO';

    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $contractAddress;

    /**
     * @var float
     */
    private $notForSaleTokens = 0.0;

    /**
     * @var float
     */
    private $totalTokens;

    /**
     * @var float
     */
    private $balance;

    /**
     * @var boolean
     */
    private $enabled = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contractAddress
     *
     * @param string $contractAddress
     *
     * @return SmartContracts
     */
    public function setContractAddress($contractAddress)
    {
        $this->contractAddress = $contractAddress;

        return $this;
    }

    /**
     * Get contractAddress
     *
     * @return string
     */
    public function getContractAddress()
    {
        return $this->contractAddress;
    }

    /**
     * Set notForSaleTokens
     *
     * @param float $notForSaleTokens
     *
     * @return SmartContracts
     */
    public function setNotForSaleTokens($notForSaleTokens)
    {
        $this->notForSaleTokens = $notForSaleTokens;

        return $this;
    }

    /**
     * Get notForSaleTokens
     *
     * @return float
     */
    public function getNotForSaleTokens()
    {
        return $this->notForSaleTokens;
    }

    /**
     * Set totalTokens
     *
     * @param float $totalTokens
     *
     * @return SmartContracts
     */
    public function setTotalTokens($totalTokens)
    {
        $this->totalTokens = $totalTokens;

        return $this;
    }

    /**
     * Get totalTokens
     *
     * @return float
     */
    public function getTotalTokens()
    {
        return $this->totalTokens;
    }

    /**
     * Set balance
     *
     * @param float $balance
     *
     * @return SmartContracts
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return SmartContracts
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isStagePreICO()
    {
        return $this->stage === self::STAGE_PRE_ICO ? true : false;
    }

    /**
     * @return bool
     */
    public function isStageICO()
    {
        return $this->stage === self::STAGE_ICO ? true : false;
    }

    /**
     * @return array
     */
    public static function getStageAsLabels()
    {
        return [
            self::STAGE_PRE_ICO => 'Pre-ICO',
            self::STAGE_ICO => 'ICO',
        ];
    }

    /**
     * @return float
     */
    public function getSoldTokens()
    {
        return $this->getTotalTokens() - $this->getNotForSaleTokens() - $this->getBalance();
    }

    /**
     * @return float
     */
    public function getIcoTokens()
    {
        return $this->getTotalTokens() - $this->getNotForSaleTokens();
    }
}
