<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class TokenController extends Controller
{
    /**
     * @Route("/token-sale", name="token_sale")
     */
    public function tokenSaleAction()
    {
	if(!$this->getUser()){
	    return $this->redirectToRoute('fos_user_security_login');
	}

        $errors = [];
        $currencies = $this->getDoctrine()->getRepository('AppBundle:CurrencyRates')->findByEnabled(true);

        $currentStage = $this->getDoctrine()->getRepository('AppBundle:IcoStages')->getCurrentStage();

        if(!empty($currentStage)){
            $nextStage = $this->getDoctrine()->getRepository('AppBundle:IcoStages')->getNextStageForCurrentStage($currentStage);
        }else{
            $nextStage = $this->getDoctrine()->getRepository('AppBundle:IcoStages')->getNextStage();
        }

        $tokenPrice = $this->getDoctrine()->getRepository('AppBundle:Settings')->getCurrentTokenPrice();

        if(!empty($tokenPrice) && $tokenPrice->getEnabled() == true){
            $roundRate = $tokenPrice->getValue();
        }else{
            $roundRate = $this->get('app.smart_contract_service')->getCurrentIcoRound()->getPrice();
        }

        return $this->render('AppBundle:Token:token_sale.html.twig', [
            'user' => $this->getUser(),
	    'errors' => $errors,
            'currentStage' => $currentStage,
            'nextStage' => !empty($nextStage) ? $nextStage : false,
            'roundRate' => $roundRate,
            'rounds' => $this->getDoctrine()->getRepository('AppBundle:IcoRounds')->getAllRounds(),
            'currencies' => $currencies,
            'chartData' => $this->get('app.landing_service')->getChartData(),
            'smartContractAddress' => $this->getDoctrine()->getRepository('AppBundle:SmartContracts')->getSmartContractAddress(),
	]);
    }

    /**
     * @Route("/withdraw", name="withdraw")
     */
    public function withdrawAction()
    {
	if(!$this->getUser()){
	    return $this->redirectToRoute('fos_user_security_login');
	}

	$errors = [];
	$translator = $this->get('translator.default');

        return $this->render('AppBundle:Token:withdraw.html.twig', [
	    'errors' => $errors,
	]);
    }

    /**
     * @Route("/calc", name="calc")
     */
    public function currencyCalculatorAction()
    {
	if(!$this->getUser()){
	    return ['sccess'];
	}

	return $this->render('AppBundle:Token:withdraw.html.twig', [
	    'errors' => $errors,
	]);
    }

    /**
     * @Route("/save-wallet", name="save_wallet", options={"expose"=true})
     */
    public function saveWalletAction(Request $request)
    {
	if(!$this->getUser()){
	    return new JsonResponse(['success' => false]);
	}
        if($request->isMethod('POST')){
            $em = $this->getDoctrine()->getManager();
            $wallet = trim($request->request->get('wallet'));
            $user = $this->getUser();
            $user->setWalletETH($wallet);
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['success' => true]);
        }

        return new JsonResponse(['success' => false]);
    }

    /**
     * @Route("/save-other-wallet", name="save_other_wallet", options={"expose"=true})
     */
    public function saveOtherWalletAction(Request $request)
    {
	if(!$this->getUser()){
	    return new JsonResponse(['success' => false]);
	}
        if($request->isMethod('POST')){
            $setter = 'setWallet'.$request->request->get('walletType');
            $em = $this->getDoctrine()->getManager();
            $wallet = trim($request->request->get('wallet'));
            $user = $this->getUser();
            $user->$setter($wallet);
            $em->persist($user);
            $em->flush();
            return new JsonResponse(['success' => true]);
        }
        
        return new JsonResponse(['success' => false]);
    }
    
    /**
     * @Route("/get-wallet", name="get_wallet", options={"expose"=true})
     */
    public function getOtherWalletAction(Request $request)
    {
	if(!$this->getUser()){
	    return new JsonResponse(['success' => false]);
	}
        if($request->isMethod('POST')){
            $getter = 'getWallet'.$request->request->get('walletType');           
            $user = $this->getUser();            
            return new JsonResponse(['success' => $user->$getter()]);
        }        
        return new JsonResponse(['success' => false]);
    }

    /**
     * @Route("/update-currency", name="update_currency", options={"expose"=true})
     */
    public function updateCurrencyAction(Request $request)
    {
        if($request->isMethod('POST') && $request->get('update') == true){
            $normalizer = new ObjectNormalizer();
            $normalizer->setIgnoredAttributes(['createdAt', 'updatedAt', 'enabled']);
            $encoder = new JsonEncoder();
            $serializer = new Serializer(array($normalizer), array($encoder));

            $getCurrencies = $this->getDoctrine()->getRepository('AppBundle:CurrencyRates')->findByEnabled(true);

            $currencies = [];
            foreach ($getCurrencies as $currency) {
                $currencies[] = $serializer->serialize($currency, 'json');
            }

            $currentRound = $this->get('app.smart_contract_service')->getCurrentIcoRound();

            return new JsonResponse([
                'success' => true,
                'roundRate' => $currentRound->getPrice(),
                'currencies' => $currencies,
            ]);
        }

        return new JsonResponse(['success' => false]);
    }
}
