<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\CurrencyRatesHistory;
use Sonata\CoreBundle\Form\Type\DateTimePickerType;

class CurrenciesHistoryAdmin extends AbstractAdmin
{
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('currency')
                ->add('rate')
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('currency')
                ->add('rate')
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
		->with('Currency', ['class' => 'col-md-12'])
                    ->add('currency')
                    ->add('rate')
                    ->add('createdAt', DateTimePickerType::class, [])
                    ->add('updatedAt', DateTimePickerType::class, [])
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Currency', ['class' => 'col-md-12'])
                    ->add('currency')
                    ->add('rate')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param Subscribers $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof IcoStages
            ? ''
            : 'Currency';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('rate')
		->assertNotBlank()
            ->end()
        ;
    }
}
