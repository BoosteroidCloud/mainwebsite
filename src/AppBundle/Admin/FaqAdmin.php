<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\Faq;

class FaqAdmin extends AbstractAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'sortOrder',
    ];

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
	$datagridMapper
		->add('enabled')
	;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
	$listMapper
		->add('sortOrder', null, ['editable' => true])
		->add('enabled', null, ['editable' => true])
                ->add('question_ru', 'html', [
		    'truncate' => ['length' => 150]
		])
		->add('answer_ru', 'html', [
		    'truncate' => ['length' => 150]
		])
		->add('question_en', 'html', [
		    'truncate' => ['length' => 150]
		])
		->add('answer_en', 'html', [
		    'truncate' => ['length' => 150]
		])
		->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
		->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
		->add('_action', null, array(
		    'actions' => array(
			'show' => array(),
			'edit' => array(),
			'delete' => array(),
		    ),
		))
	;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
	$formMapper
                ->tab('Settings')
                    ->with('Display Settings', ['class' => 'col-md-2'])
                        ->add('sortOrder')
                        ->add('enabled')
                    ->end()
                ->end()
                ->tab('Russian')
                    ->with('Question', ['class' => 'col-md-5'])
                        ->add('question_ru', CKEditorType::class, [
                            'config_name' => 'default_config'
                        ])
                    ->end()
                    ->with('Answer', ['class' => 'col-md-5'])
                        ->add('answer_ru', CKEditorType::class, [
                            'config_name' => 'default_config'
                        ])
                    ->end()
                ->end()
                ->tab('English')
                    ->with('Question', ['class' => 'col-md-5'])
                        ->add('question_en', CKEditorType::class, [
                            'config_name' => 'default_config'
                        ])
                    ->end()
                    ->with('Answer', ['class' => 'col-md-5'])
                        ->add('answer_en', CKEditorType::class, [
                            'config_name' => 'default_config'
                        ])
                    ->end()
                ->end()
	;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
	$showMapper
                ->with('Russian', ['class' => 'col-md-6'])
                    ->add('question_ru', 'html')
                    ->add('answer_ru', 'html')
                ->end()
                ->with('English', ['class' => 'col-md-6'])
                    ->add('question_en', 'html')
                    ->add('answer_en', 'html')
                ->end()
                ->with('Display Settings', ['class' => 'col-md-12'])
                    ->add('id')
                    ->add('sortOrder')
                    ->add('enabled')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()

        ;
    }

    /**
     * @param Faq $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Faq
            ? 'Question '.$object->getId()
            : 'New Question';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('sortOrder')
		->assertType([
		    'type' => 'integer',
		])
	    ->end()
            ->with('question_ru')
		->assertLength([
		    'min' => 10,
		])
	    ->end()
            ->with('answer_ru')
		->assertLength([
		    'min' => 10,
		])
            ->end()
            ->with('question_en')
		->assertLength([
		    'min' => 10,
		])
	    ->end()
            ->with('answer_en')
		->assertLength([
		    'min' => 10,
		])
            ->end()
        ;
    }
}