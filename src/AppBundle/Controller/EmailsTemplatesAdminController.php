<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;

class EmailsTemplatesAdminController extends CRUDController
{
    /**
     * @param Request $request
     * @return array
     * @throws NotFoundException
     * @throws NotFoundHttpException
     */
    public function sentTemplateAction(Request $request)
    {
	$id = $request->request->get('id');

	if (!$request->isXmlHttpRequest() && !$id) {
	    throw $this->createNotFoundException('404');
	}

	if ($request->isMethod('POST')) {
	    $translator = $this->get('translator');
	    $object = $this->getDoctrine()->getRepository('AppBundle:EmailsTemplates')->find($id);
            if (!$object) {
		throw new NotFoundHttpException(sprintf('Unable to find the object with id: %s', $id));
	    }
            if ($object) {
                $this->container->get('app.mailer_service')->sendTemplateToUsersMail($object);
                $this->addFlash('sonata_flash_success','emails are sent');
                return new JsonResponse(['success' => true]);
            }
	}

	$this->addFlash('sonata_flash_error','error');
	return new JsonResponse(['success' => false]);
    }
}
