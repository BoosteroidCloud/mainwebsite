<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * Faq
 */
class Faq
{
    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question_en;

    /**
     * @var string
     */
    private $answer_en;

    /**
     * @var string
     */
    private $question_ru;

    /**
     * @var string
     */
    private $answer_ru;

    /**
     * @var integer
     */
    private $sortOrder;

    /**
     * @var boolean
     */
    private $enabled = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionEn
     *
     * @param string $questionEn
     *
     * @return Faq
     */
    public function setQuestionEn($questionEn)
    {
        $this->question_en = $questionEn;

        return $this;
    }

    /**
     * Get questionEn
     *
     * @return string
     */
    public function getQuestionEn()
    {
        return $this->question_en;
    }

    /**
     * Set answerEn
     *
     * @param string $answerEn
     *
     * @return Faq
     */
    public function setAnswerEn($answerEn)
    {
        $this->answer_en = $answerEn;

        return $this;
    }

    /**
     * Get answerEn
     *
     * @return string
     */
    public function getAnswerEn()
    {
        return $this->answer_en;
    }

    /**
     * Set questionRu
     *
     * @param string $questionRu
     *
     * @return Faq
     */
    public function setQuestionRu($questionRu)
    {
        $this->question_ru = $questionRu;

        return $this;
    }

    /**
     * Get questionRu
     *
     * @return string
     */
    public function getQuestionRu()
    {
        return $this->question_ru;
    }

    /**
     * Set answerRu
     *
     * @param string $answerRu
     *
     * @return Faq
     */
    public function setAnswerRu($answerRu)
    {
        $this->answer_ru = $answerRu;

        return $this;
    }

    /**
     * Get answerRu
     *
     * @return string
     */
    public function getAnswerRu()
    {
        return $this->answer_ru;
    }

    /**
     * Set sortOrder
     *
     * @param integer $sortOrder
     *
     * @return Faq
     */
    public function setSortOrder($sortOrder)
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get sortOrder
     *
     * @return integer
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return Faq
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get questionRu
     *
     * @param string $locale
     * @return string
     */
    public function getQuestion($locale)
    {
        switch ($locale):
            case 'ru_RU':
                $question = $this->question_ru;
            break;
            case 'en_EN':
                $question = $this->question_en;
            break;
            default:
                $question = $this->question_en;
            break;
        endswitch;

        return $question;
    }

    /**
     * Get answer
     *
     * @param string $locale
     * @return string
     */
    public function getAnswer($locale)
    {
        switch ($locale):
            case 'ru_RU':
                $answer = $this->answer_ru;
            break;
            case 'en_EN':
                $answer = $this->answer_en;
            break;
            default:
                $answer = $this->answer_en;
            break;
        endswitch;

        return $answer;
    }
}
