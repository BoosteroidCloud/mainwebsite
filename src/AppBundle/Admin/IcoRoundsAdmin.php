<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\IcoRounds;

class IcoRoundsAdmin extends AbstractAdmin
{
    /**
     * @return array
     */
    public function getIcoStages()
    {
	return $this->getConfigurationPool()->getContainer()->get('doctrine.orm.default_entity_manager')->getRepository('AppBundle:IcoStages')->findAll();
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('icoStage', null, [], 'entity', [
		    'class' => 'AppBundle:IcoStages',
		    'choices' => $this->getIcoStages(),
		    'choice_label' => function ($object) {
			return $object->getStage();
		    },
                    'label' => 'ICO Stage',
		])
		->add('round', null, [], 'choice', [
                    'choices' => IcoRounds::STAGES_ROUNDS,
                ])
                ->add('price')
                ->add('tokensAmount')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('icoStage.stage', null, [
                    'label' => 'ICO Stage',
                ])
                ->add('round')
                ->add('price')
                ->add('tokensAmount')
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
        	->with('Ico Stage', ['class' => 'col-md-6'])
                    ->add('icoStage', 'entity', [
			'class' => 'AppBundle:IcoStages',
			'required' => true,
			'placeholder' => 'Select Ico Stage',
			'choices' => $this->getIcoStages(),
			'choice_label' => function ($object) {
			    return $object->getStage();
			}
		    ])
                    ->add('round', 'choice', [
                        'choices' => IcoRounds::STAGES_ROUNDS,
                    ])
                    ->add('price')
                    ->add('tokensAmount')
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Ico Stage', ['class' => 'col-md-6'])
                    ->add('icoStage.stage')
                    ->add('round')
                    ->add('price')
                    ->add('tokensAmount')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param Subscribers $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof IcoRounds && $object->getIcoStage() != null
                ? $object->getIcoStage()->getStage(). ' Round ' . $object->getRound()
                : 'New Ico Round';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('icoStage')
		    ->assertNotBlank()
                    ->assertChoice([
                        'choices' => $this->getIcoStages(),
                    ])
                ->end()
                ->with('round')
		    ->assertNotBlank()
                    ->assertChoice([
                        'choices' => IcoRounds::STAGES_ROUNDS,
                    ])
                ->end()
                ->with('price')
                    ->assertNotBlank()
                    ->assertType([
                        'type' => 'numeric',
                    ])
                ->end()
                ->with('tokensAmount')
                    ->assertNotBlank()
                    ->assertType([
                        'type' => 'numeric',
                    ])
                ->end()
        ;
    }
}
