<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
	$builder
		->add('firstName', Type\TextType::class, [
		    'required' => true,
		    'label' => 'First Name',
		    'error_bubbling' => true,
		    'constraints' => [
			new Assert\NotBlank(),
			new Assert\Type([
			    'type' => 'string',
			]),
			new Assert\Length([
			    'min' => 3,
			]),
		    ]
		])
		->add('lastName', Type\TextType::class, [
		    'required' => true,
		    'label' => 'Last Name',
		    'error_bubbling' => true,
		    'constraints' => [
			new Assert\NotBlank(),
			new Assert\Type([
			    'type' => 'string',
			]),
			new Assert\Length([
			    'min' => 3,
			]),
		    ]
		])
		->add('email', Type\EmailType::class, [
		    'required' => true,
		    'error_bubbling' => true,
		    'constraints' => [
			new Assert\NotBlank(),
			new Assert\Email([
			    'checkHost' => true,
			]),
			new Assert\Length([
			    'min' => 7,
			]),
		    ]
		])
//		->add('phone', Type\TextType::class, [
//		    'required' => true,
//		    'error_bubbling' => true,
//		    'constraints' => [
//			new Assert\NotBlank(),
//			new Assert\Type([
//			    'type' => 'string',
//			]),
//			new Assert\Length([
//			    'min' => 12,
//			]),
//		    ]
//		])
	;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
	$resolver->setDefaults(array(
	    'data_class' => 'AppBundle\Entity\Users'
	));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
	return 'app_user';
    }
}
