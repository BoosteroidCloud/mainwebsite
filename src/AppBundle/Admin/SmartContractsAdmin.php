<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Entity\SmartContracts;

class SmartContractsAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
		->remove('create')
		->remove('delete')
	;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('contractAddress')
                ->add('enabled')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('id')
                ->add('contractAddress')
                ->add('notForSaleTokens')
                ->add('totalTokens')
                ->add('balance')
                ->add('enabled', null, ['editable' => true])
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Smart Contract', ['class' => 'col-md-6'])
                    ->add('contractAddress')
                    ->add('notForSaleTokens', 'number')
                    ->add('totalTokens', 'number')
                    ->add('balance', 'number')
                    ->add('enabled')
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Smart Contract', ['class' => 'col-md-6'])
                    ->add('id')
                    ->add('contractAddress')
                    ->add('notForSaleTokens')
                    ->add('totalTokens')
                    ->add('balance')
                    ->add('enabled')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param SmartContracts $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof SmartContracts
            ? 'Smart Contract '.$object->getId()
            : 'New Smart Contract';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('contractAddress')
                ->assertNotBlank()
		->assertType([
		    'type' => 'string',
		])
                ->assertLength([
		    'min' => 4,
		])
	    ->end()
            ->with('notForSaleTokens')
                ->assertNotBlank()
		->assertType([
		    'type' => 'numeric',
		])
	    ->end()
            ->with('totalTokens')
                ->assertNotBlank()
                ->assertNotNull()
                ->assertNotEqualTo([
                    'value' => 0
                ])
		->assertType([
		    'type' => 'numeric',
		])
	    ->end()
            ->with('balance')
		->assertType([
		    'type' => 'numeric',
		])
	    ->end()
        ;
    }
}