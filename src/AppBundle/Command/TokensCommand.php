<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class TokensCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
                ->setName('app:tokens:remain')
                ->setDescription('Parse number of smart contract tokens');
    }

    /**
     * Update ramein tokens of smart contract and store it in DB
     * 
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getContainer()->get('app.smart_contract_service')->setRamainTokens();
    }

}
