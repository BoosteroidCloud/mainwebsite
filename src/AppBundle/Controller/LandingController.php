<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Form\SubscribersType;
use AppBundle\Entity\Subscribers;
use AppBundle\Entity\IcoRounds;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LandingController extends Controller
{
    /**
     * @param Request $request
     */
    public function landingPageAction(Request $request)
    {
	//$questions = $this->getDoctrine()->getRepository('AppBundle:Faq')->findBy(['enabled' => true], ['sortOrder' => 'ASC']);
        $startIcoRounds = $this->getDoctrine()->getRepository('AppBundle:IcoRounds')->findBy(['round' => IcoRounds::STAGES_ROUNDS[1]], ['icoStage' => 'ASC']);
        $checkInvested = $this->getDoctrine()->getRepository('AppBundle:Settings')->findOneByParamKey('invested_amount');
        $form = $this->createForm(SubscribersType::class);
        $form->handleRequest($request);

        return $this->render('AppBundle:Landing:landing_page.html.twig', [
	    //'questions' => $questions,
            'first_form' => $form->createView(),
            'second_form' => $form->createView(),
            'investedAmount' =>$checkInvested->getEnabled() ? $this->getDoctrine()->getRepository('AppBundle:Settings')->getInvestedAmount():null,
	    'startIcoRounds' => $startIcoRounds,
	    'chartData' => $this->get('app.landing_service')->getChartData(),
	]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws NotFoundException
     */
    public function subscribeAction(Request $request)
    {
	if (!$request->isXmlHttpRequest()) {
	    throw $this->createNotFoundException('404');
	}

	$form = $this->createForm(SubscribersType::class);
	$form->handleRequest($request);

	if($request->isMethod('POST') && $form->isValid()){
            $service = $this->get('app.landing_service');
            $subscriber = $service->setSubscriber($form->getData(), $request->getLocale());
            if($subscriber !== false && $subscriber instanceof Subscribers){
                $subscriber = $service->addConfirmationToken($subscriber);
                if($subscriber){
                    $this->container->get('app.mailer_service')->subscribeMail($subscriber);
                    return new JsonResponse(['success' => true]);
                }
            }
		}

        if($form->getErrors()){
	    $errors = $form->getErrors();
            return new JsonResponse(['success' => false, 'errors' => $errors]);
		}
        return new JsonResponse(['error' => true]);
    }

    /**
     * @param string $token
     * @return RedirectResponse
     */
    public function confirmSubscribeAction($token)
    {
        if($token){
            $subscriber = $this->getDoctrine()->getRepository('AppBundle:Subscribers')->findOneByConfirmationToken($token);
            if($subscriber){
                $this->container->get('app.mailer_service')->subscribeAddToListMail($subscriber);

                if($this->get('app.landing_service')->activateSubscriber($subscriber, $token)){
                    $this->redirectToRoute('landing_page');
                }
            }
        }

        return $this->redirectToRoute('landing_page');
    }

    public function whitePapersAction(Request $request)
    {
        /**
         * @todo one template
         */
        if ($request->getLocale() == 'en_EN') {
            return $this->render('AppBundle:WhitePaper:white_paper_en.html.twig', []);
        } elseif ($request->getLocale() == 'zh_CN'){
            return $this->render('AppBundle:WhitePaper:white_paper_ch.html.twig', []);
        } elseif ($request->getLocale() == 'ja_JP'){
            return $this->render('AppBundle:WhitePaper:white_paper_ja.html.twig', []);
        } else {
            return $this->render('AppBundle:WhitePaper:white_paper_ru.html.twig', []);
        }
    }

    public function bountyCampaingAction(Request $request)
    {
        return $this->redirectToRoute('landing_page');
//        if($request->getLocale()=='en_EN') {
//           return $this->render('AppBundle:Bounty:bounty_en.html.twig', []); 
//        }else{
//           return $this->render('AppBundle:Bounty:bounty_ru.html.twig', []);  
//        }
    }

    public function testModeStubAction()
    {
	return $this->render('AppBundle:Landing:test_mode_stub.html.twig');
    }
}
