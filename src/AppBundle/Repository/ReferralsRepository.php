<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Referrals;
use AppBundle\Entity\Users;

/**
 * ReferralsRepository
 */
class ReferralsRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Users $user
     * @param Users $referrralUser
     */
    public function addReferral(Users $user, Users $referrralUser)
    {
        $em = $this->getEntityManager();
        $referrral = new Referrals();
        $referrral
                ->setUser($user)
                ->setRefferalUserId($referrralUser->getId())
        ;

        $em->persist($referrral);
        $em->flush();

        return $referrral;
    }

    /**
     * @param Users $user
     * @return array
     */
    public function getActiveReferrals(Users $user)
    {
        $qb = $this->createQueryBuilder('ref');
        $query = $qb
                ->select($qb->expr()->count('ref'))
                ->leftJoin('AppBundle\Entity\Users', 'ur', 'WITH', 'ref.refferalUserId = ur.id')
                ->leftJoin('ref.user', 'u', 'WITH', 'ref.user = u.id')
                ->where('u.id = :userId')
                ->andWhere('ur.enabled = 1')
                ->setParameter('userId', $user->getId())
                ->getQuery()
                ->getSingleScalarResult()
        ;

        return $query;
    }
    
    /**
     * @param array $user
     * @return array
     */
    public function getSingleUserReferrals($user)
    {
        $qb = $this->createQueryBuilder('ref');
        $query = $qb->select('ur.email, ur.id, ur.enabled, ur.createdAt')
                ->leftJoin('AppBundle\Entity\Users', 'ur', 'WITH', 'ref.refferalUserId = ur.id')
                ->leftJoin('ref.user', 'u', 'WITH', 'ref.user = u.id')
                ->where('u.id = :userId')
                ->setParameter('userId', $user->getId())
                ->getQuery()
                ->getResult()
        ;

        return $query;
    }
    /**
     * @param array $user
     * @return array
     */
    public function getUserReferrals($users)
    {
        $array = [];
        foreach ($users as $value) {
            $array[$value->getEmail().' '.'id='.$value->getId()]=$this->getSingleUserReferrals($value);
        } 
        return $array;
    }
    
}
