<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class InformationController extends Controller
{
    /**
     * @Route("/faq", name="faq")
     */
    public function faqAction()
    {
	//$questions = $this->getDoctrine()->getRepository('AppBundle:Faq')->findBy(['enabled' => true], ['sortOrder' => 'ASC']);
        return $this->render('AppBundle:Information:faq.html.twig', [
	    //'questions' => $questions,
	]);
    }
}
