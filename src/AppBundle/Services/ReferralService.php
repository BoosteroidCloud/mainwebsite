<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use AppBundle\Entity\Users;
use AppBundle\Entity\Referrals;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class ReferralService
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
	$this->container = $container;
	$this->em = $em;
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $route         The name of the route
     * @param mixed  $parameters    An array of parameters
     * @param int    $referenceType The type of reference (one of the constants in UrlGeneratorInterface)
     *
     * @return string The generated URL
     *
     * @see UrlGeneratorInterface
     */
    protected function generateUrl($route, $parameters = array(), $referenceType = UrlGeneratorInterface::ABSOLUTE_PATH)
    {
        return $this->container->get('router')->generate($route, $parameters, $referenceType);
    }

    /**
     * @param Users $user
     * @return string
     */
    public function generateReferralLink(Users $user)
    {
        $token = md5(($user->getEmail().$user->getCreatedAt()->format('d/m/Y H:i:s')));
        return $token;
    }

    /**
     * @param Users $referrralUser
     * @param string $token
     * @return boolean
     */
    public function addReferral(Users $referrralUser, $token)
    {
        $user = $this->em->getRepository('AppBundle:Users')->findOneByReferralToken($token);
        if(!empty($user)){
            $this->em->getRepository('AppBundle:Referrals')->addReferral($user, $referrralUser);
            return true;
        }
        return;
    }
}
