<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use AppBundle\Entity\CurrencyRates;

class CurrencyRatesAdmin extends AbstractAdmin
{
    
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('currency')
                ->add('rate')
                ->add('enabled')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('currency')
                ->add('rate')
                ->add('enabled', null, ['editable' => true])
                ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
		->with('Currency', ['class' => 'col-md-12'])
                    ->add('currency')
                    ->add('rate')
                    ->add('enabled')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Currency', ['class' => 'col-md-12'])
                    ->add('currency')
                    ->add('rate')
                    ->add('enabled')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param Subscribers $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof CurrencyRates
            ? $object->getCurrency()
            : 'new Currency Rate';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
            ->with('rate')
		->assertNotBlank()
                ->assertType([
                    'type' => 'numeric',
                ])
            ->end()
            ->with('currency')
		->assertNotBlank()
		->assertLength([
		    'max' => 3,
		])
            ->end()
        ;
    }
}
