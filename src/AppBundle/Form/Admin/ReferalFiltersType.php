<?php

namespace AppBundle\Form\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as Type;
use Symfony\Component\Validator\Constraints as Assert;
use Sonata\CoreBundle\Form\Type\DatePickerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class ReferalFiltersType extends AbstractType
{    
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('start', DatePickerType::class, [
		    'error_bubbling' => true,
		    'required' => true,
		    'attr' => [
			'class' => 'form-control',
		    ],
		    'format' => 'dd/MM/yyyy',
		    'dp_show_today' => true,
		])
                ->add('end', DatePickerType::class, [
		    'error_bubbling' => true,
		    'required' => true,
		    'attr' => [
			'class' => 'form-control',
		    ],
		    'format' => 'dd/MM/yyyy',
		    'dp_show_today' => true,
		])
//		->add('users', EntityType::class, [
//		    'class' => 'AppBundle:Users',
//                    'query_builder' => function (EntityRepository $er) {
//                        return $er->createQueryBuilder('t')
//                            ->orderBy('t.email', 'ASC');
//                    },
//		    'choice_label' => function ($object) {
//			return $object->getEmail();
//		    },
//		    'error_bubbling' => true,
//		    'required' => false,
//		    'attr' => [
//			'class' => 'form-control',
//			'autocomplete' => 'off',
//		    ],
//		])
	;
    }    

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_referals_filters';
    }
}
