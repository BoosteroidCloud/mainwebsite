<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Entity\Settings;

class SettingsAdmin extends AbstractAdmin
{
    /**
     * @var array 
     */
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'paramGroup',
    ];

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
                ->add('paramGroup')
                ->add('paramKey')
                ->add('value')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
                ->add('paramGroup')
                ->add('paramKey')
                ->add('value')
                ->add('enabled', null, ['editable' => true])
                ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->add('_action', null, array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $obj = $this->getSubject();
        $paramOptions = [];

        if($obj->getId()){
            $paramOptions['attr']['readOnly'] = true;
        }

        $formMapper
		->with('Settings Data', ['class' => 'col-md-6'])
                    ->add('paramGroup', null, $paramOptions)
                    ->add('paramKey', null, $paramOptions)
                    ->add('value')
                    ->add('enabled')
                ->end()
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
                ->with('Setting', ['class' => 'col-md-6'])
                    ->add('paramGroup')
                    ->add('paramKey')
                    ->add('value')
                    ->add('enabled')
                    ->add('createdAt', null, ['format' => 'd/m/Y H:i:s'])
                    ->add('updatedAt', null, ['format' => 'd/m/Y H:i:s'])
                ->end()
        ;
    }

    /**
     * @param Settings $object
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Settings
            ? $object->getParamKey()
            : 'Setting';
    }

    /**
     * @param ErrorElement $errorElement
     * @param object $object
     */
    public function validate(ErrorElement $errorElement, $object)
    {
        $errorElement
                ->with('paramGroup')
                    ->assertNotBlank()
                ->end()
                ->with('paramKey')
                    ->assertNotBlank()
                ->end()
        ;

        if($object->getId() && $object->isInvestedAmount()){
            $errorElement
                ->with('value')
                    ->assertNotBlank()
                    ->assertType([
                        'type' => 'numeric',
                    ])
                    ->assertGreaterThanOrEqual([
                        'value' => 0,
                    ])
                ->end()
            ;
        }elseif($object->getId() && $object->isCurrentTokenPrice()){
            $errorElement
                ->with('value')
                    ->assertNotBlank()
                    ->assertType([
                        'type' => 'numeric',
                    ])
                    ->assertGreaterThanOrEqual([
                        'value' => 0,
                    ])
                ->end()
            ;
        }else{
            $errorElement
                ->with('value')
                    ->assertNotBlank()
                ->end()
            ;
        }
    }
}
