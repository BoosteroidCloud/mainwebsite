$(function(){
    $('.language_select').click(function(){
        $(this).toggleClass('open');
    });
});

function change_lang(locale) {
    $.get(Routing.generate('language'), {'locale': locale}).done(function (data) {
        if (data.change) {
            location.reload();
        }
    });
}