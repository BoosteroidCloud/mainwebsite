$(document).ready(function(){

    $(".toggle_menu").click(function() {
        $(this).toggleClass("on");
        $("nav").slideToggle();
    });

  
    window.setInterval(countDown, 500);

    function countDown() {
        var now = new Date();
        var future = new Date("10/30/2017 12:00:00");
        var timeLeft = future - now;
        var milli = timeLeft;

        var seconds = milli / 1000;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var spareSeconds = seconds % 60;
        var spareMinutes = minutes % 60;
        var spareHours = hours % 24;
        var spareDays = days % 365;

        minutes = parseInt(minutes);
        hours = parseInt(hours);
        days = parseInt(days);
        spareSeconds = parseInt(spareSeconds);
        spareMinutes = parseInt(spareMinutes);
        spareHours = parseInt(spareHours);
        spareDays = parseInt(spareDays);

        // days = padNumber(days);
        // hours = padNumber(hours);
        // minutes = padNumber(minutes);
        // spareSeconds = padNumber(spareSeconds);
        // spareMinutes = padNumber(spareMinutes);
        // spareHours = padNumber(spareHours);
        // spareDays = padNumber(spareDays);

        // timeLeft = spareDays + ":" + spareHours + ":" + spareMinutes + ":" + spareSeconds;
        var mySpan = document.getElementById("timer");
        var myseconds = document.querySelector('.seconds').innerHTML = spareSeconds;
        var myminutes = document.querySelector('.minutes').innerHTML = spareMinutes;
        var myhours = document.querySelector('.hours').innerHTML = spareHours;
        var mydays = document.querySelector('.days').innerHTML = spareDays;
        var myseconds_f = document.querySelector('.seconds_f').innerHTML = spareSeconds;
        var myminutes_f = document.querySelector('.minutes_f').innerHTML = spareMinutes;
        var myhours_f = document.querySelector('.hours_f').innerHTML = spareHours;
        var mydays_f = document.querySelector('.days_f').innerHTML = spareDays;
        // myseconds.innerHTML = spareSeconds;
        // myminutes.innerHTML = minutes;
        // myhours.innerHTML = hours;
        // mydays.innerHTML = days;
        // mySpan.innerHTML = timeLeft;
        // var myFooterSpan = document.getElementById("footer_timer");
        // myFooterSpan.innerHTML = timeLeft;

        var timer_f = $('#timer_f');

        if (milli <= 0) { //Time's run out! If all values go to zero
            var timer_f = $('#timer_f');
          mySpan.innerHTML = "00:00:00";
          timer_f.innerHTML = "00:00:00";
        }
    }

setTimeout(() => $('h1').css({ 'display': 'block'}), 7000);




// $(window).scroll(function() {
//         var top = $(document).scrollTop();

//         $('.slogan p').animate({'margin-top':'9000px', opacity:0}, 3000);
//         $('.main').animate({'opacity':'1'}, 3000);
//         $('.main').css({ 'display':' block'}, 3000);
//         // $('h1').animate({ 'width':' 100%'}, 3000);
//         $('h1').css({ 'display':' block'}, 3000);
//         $('body').css({ 'top':' 0', 'position':'reletive'}, 3000);
//         return false;
//     });



var ctx = document.getElementById('chart').getContext("2d");

var gradientStrokeMain = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeMain.addColorStop(1, '#00c6ff');
gradientStrokeMain.addColorStop(0, '#5c36ee');

var gradientStrokeAlternate = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeAlternate.addColorStop(1, '#1c374f');
gradientStrokeAlternate.addColorStop(0, '#fcfdfd');

var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["", "5.09", "19.09", "2018", "2019"],
        datasets: [{
            label: "Boosteroid",
            lineTension: 0,
            borderColor: gradientStrokeMain,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeMain,
            // pointHoverBorderColor: gradientStrokeMain,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 6,
            data: [0.5, 0.5, 2.5, 5.5, 6.5]
        },
        {
            label: "AWS",
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",                       
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [12, 12, 12, 12.5, 13]
        },
        {
            label: "Microsoft",
            lineTension: 0,
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [8.5, 9, 9, 9, 10]
        },
        {
            label: "Flyelephant",
            lineTension: 0,
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [5.5, 6.5, 7.5, 7.5, 8.5]
        }
      ]
    },
    options: {
        animation: {
          easing: "easeInOutBack"
        },
        legend: {
            position: "bottom"
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    // maxTicksLimit: 5,
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                },
                gridLines: {
                    drawTicks: true,
                    display: true,
                    zeroLineColor: "#223b50",
                    color: "#0e2b44"
                }

            }],
            xAxes: [{
                gridLines: {
                    zeroLineColor: "transparent",
                    color: "#0e2b44"
                },
                ticks: {
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                }
            }]
        }
    }
});

$('#faq li > .item').click(function(){
        $(this).toggleClass('active');
        $(this).next('div').slideToggle(200);
    });





//     function padNumber(number) {
//       if (number < 10) {
//         number = "0" + number;
//       }
//       return number;
//     }

    $("header nav").on("click","a[href^='#']", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
            top = $(id).offset().top;  
            $('body,html').animate({scrollTop: top}, 1500);
    });

    $(".footer_nav ul").on("click","a[href^='#']", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;  
        $('body,html').animate({scrollTop: top}, 1500);
    });


//     $(".top").click(function(){
//         $("html, body").animate({scrollTop: 0}, "slow");
//         return false;
//     });


//     $('.language-select').click(function(){
//         $(this).toggleClass('open');
//     });

//     $('.container').click(function(){
//         $(this).toggleClass('flipped');
//     });

//     

//     $('.gannt').click(function(){
//         $('#gannt').fadeIn();
//         if (window.innerWidth>1000) {
//             q_width = (window.innerWidth - $('#gannt').width())/2;
//         } else q_width = 0;
//         q_height = (window.innerHeight - $('#gannt').height())/2;
//         $('#gannt').css({
//             'left': q_width,
//             'top': q_height
//         });
//         $('body').append('<div id="fade"></div>');
//         $('#fade').css({'filter' : 'alpha(opacity=40)'}).fadeIn();
//         return false;
//     });

//     $("#popup_close").click(function(){
//         $('#gannt').fadeOut();
//         $('#fade').remove();
//     });
//     $(document).mouseup(function(e){
//         var container = $("#gannt");
//         if (container.has(e.target).length === 0){
//             $('#gannt').fadeOut();
//             $('#fade').remove();
//         };
//     });
//     $("#gannt").mouseup(function(e){
//         var container = $("#gannt");
//         if (container.has(e.target).length === 0){
//             return false;
//         };
//     });

//     // google.charts.load('current', {'packages':['corechart']});
//     // google.charts.setOnLoadCallback(drawChart);
//     // function drawChart() {
//     //     var percentage = google.visualization.arrayToDataTable([
//     //         ["Structure", "Percent"],
//     //         ["Supercomputer and infrastructure", 80.0],
//     //         ["Development", 10.0],
//     //         ["Marketing and PR", 7.0],
//     //         ["Research", 3.0]
//     //     ]);
//     //     var options = {
//     //         legend: {
//     //             position: "labeled",
//     //             textStyle: {fontSize: 22, fontName: "OpenSansRegular"}
//     //         },
//     //         chartArea: {width: "100%", height: "80%"},
//     //         pieSliceText: "none",
//     //         colors: ["#39d9b6", "#f4f083", "#78f8cf", "#a374f0"],
//     //         tooltip: { trigger: 'none' },
//     //         pieHole: 0
//     //     };
//     //     var chart = new google.visualization.PieChart(document.getElementById("piechart"));
//     //     var redrawChart = function () {
//     //         options.legend.position = (window.outerWidth < 768 ? "bottom" : "labeled");
//     //         options.pieSliceText = (window.outerWidth < 768 ? "percentage" : "none");
//     //         chart.draw(percentage, options);
//     //     };
//     //     redrawChart();

//     //     if (window.addEventListener) {
//     //         window.addEventListener("resize", redrawChart);
//     //     } else {
//     //         window.attachEvent("onresize", redrawChart);
//     //     }
//     // };



//     $(".subscribe_email").submit(function() {
//         var form_data = $(this).serialize();
//         $.ajax({
//             type: "POST",
//             url: "mail.php",
//             data: form_data,
//             success: function() {
//                 $('.subscribe_email').html("<br/>Thank you. The application was accepted.");
//             }
//         });
//     });


// /*
// *
// *   Currency Converter
// *
// */



//     var input = document.getElementById('amount_TFL_to_buy');
//     var p = document.querySelector('.box_qty_me');
//     var li = document.querySelectorAll('.selectric_items li');
//     var controls = document.querySelector('.box_qty_form_btn');
//     var selectActive = document.querySelector('.selected').innerHTML;
//     var total_money = document.querySelector('.total_money span');
//     // console.log(total_money);

//     var q = [1325, 21, 111.5, 92, 2];
//     var currency = [' BTC', ' LTC', ' ETH', ' DASH', ' USD'];


//     $('.selectric').click(function(){
//         $('.selectric_items').slideToggle();
//         return false;
//     });

//     $('.val_in').click(function(){
//         if(!$(this).hasClass('selected')){ 
//             $('.val_in').removeClass('selected').removeClass('highlighted');
//             $(this).addClass('selected').addClass('highlighted'); 
//             var labeSelectric = document.querySelector('.selectric .label');
//             var selectActive = document.querySelector('.selected').innerHTML;
//             var selectActive2 = labeSelectric.innerHTML = selectActive;
//         }
//         var curs = $('.val_in.selected');
//         var span_curs = $('.curs');
//         if(curs.hasClass('inbtc')){
//             span_curs.html((Math.round(1/q[0] * 10000) / 10000)+currency[0]);
//         } else if(curs.hasClass('inltc')){
//             span_curs.html((Math.round(1/q[1] * 10000) / 10000)+currency[1]);
//         } else if(curs.hasClass('ineth')){
//             span_curs.html((Math.round(1/q[2] * 10000) / 10000)+currency[2]);
//         } else if(curs.hasClass('indash')){
//             span_curs.html((Math.round(1/q[3] * 10000) / 10000)+currency[3]);
//         } else if(curs.hasClass('inusd')){
//             span_curs.html((Math.round(1/q[4] * 10000) / 10000)+currency[3]);
//         }
//     });




    
//     function bindOutput(targetElement, targetElementTotal) {
//         return function(newValue) {
//             var tElement = targetElement.innerHTML = newValue*0.2^0;
//             var tElementTotal = targetElementTotal.innerHTML  = +tElement + +input.value;
//             console.log(tElementTotal);
//         };
//     }
//     // function bindTotal(targetElement) {
//     //     return function(newValue) {
//     //         targetElement.innerHTML = newValue*0.2^0;;
//     //     };
//     // }
//     function bind2Output(tElement) {
//         return function() {
//             var inputValue = +(input.value);
//             for (var i = 0; i < tElement.length; i++) {
//                 var element = (inputValue/q[i]);
//                 element = (Math.round(element * 10000) / 10000) + currency[i];

//                 var num = !isNaN(inputValue);
//                 tElement[i].innerHTML = num ? element : 0 + currency[i];
//             }
//             return tElement;       
//         };
//     }
//     function getCallbacks(inputElement, outputCallback, currency) {
//         return {
//             plus: function(event) {
//                 var num = !isNaN(+inputElement.value)&&(+inputElement.value<10000000);
//                 inputElement.value = num ? +inputElement.value + 100 : 1;
//                 outputCallback(input.value);
//                 currency();
//                 selectAct();
//             },
//             minus: function (event) {
//                 var num = !isNaN(+inputElement.value)&&(+inputElement.value>100);
//                 inputElement.value = num ? +inputElement.value - 100 : 1;
//                 outputCallback(input.value);
//                 currency();
//                 selectAct();
//             }
//         };
//     }
//     function mapEventToValue(event) {
//         return event.target.value;
//     }
//     function compose() {
//         var args = Array.prototype.slice.call(arguments);   
//         return function(initValue) {
//             return args.reduce(function(acc, el) {                
//                 return el(acc);
//             }, initValue);
//         };
//     }
//     function selectAct(){
//         var selectActive = document.querySelector('.selected').innerHTML;
//         var labeSelectric = document.querySelector('.selectric .label');
//         return labeSelectric.innerHTML = selectActive;
//     }
   
//     var pOutput = bindOutput(p, total_money);
//     var liOutput = bind2Output(li);
//     var callbacks = getCallbacks(input, pOutput, liOutput);
//     input.addEventListener('input', compose(mapEventToValue, pOutput));
//     input.addEventListener('input', compose(mapEventToValue, liOutput));
//     input.addEventListener('input', function selectAct(){
//         var selectActive = document.querySelector('.selected').innerHTML;
//         var labeSelectric = document.querySelector('.selectric .label');
//         return labeSelectric.innerHTML = selectActive;
//     });
//     controls.addEventListener('click', function(event) {
//         event.preventDefault();
//         var controlType = event.target.getAttribute('data-control');
//         typeof callbacks[controlType] === 'function' && callbacks[controlType]();
//     });


//     $(".slider_phone.owl-carousel").owlCarousel({
//         items : 1,
//         nav : true,
//         navText : " ",
//         loop : true,
//         margin: 0,
//         autoplay : true,
//         autoplayHoverPause : true,
//         fluidSpeed : 900,
//         autoplaySpeed : 900,
//         navSpeed : 200,
//         dotsSpeed : 100,
//         dragEndSpeed : 900
//     });

//     if(window.innerWidth <= 768){
//         $(".people").addClass("owl-carousel");
//     } else{
//         $(".people").removeClass("owl-carousel");
//     }

    
//     $(".people.owl-carousel").owlCarousel({
//         items : 2,
//         nav : true,
//         navText : " ",
//         loop : true,
//         margin: 10,
//         autoplay : true,
//         autoplayHoverPause : true,
//         fluidSpeed : 900,
//         autoplaySpeed : 900,
//         navSpeed : 200,
//         dotsSpeed : 100,
//         dragEndSpeed : 900,
//         responsive:{ 
//             0:{
//                 items:1
//             },
//             480:{
//                 items:2
//             }
//         }
//     });

//     window.onresize = function(){
//         if(window.innerWidth <= 768){
//             $(".people").addClass("owl-carousel").owlCarousel({
//                 items : 2,
//                 nav : true,
//                 navText : " ",
//                 loop : true,
//                 margin: 10,
//                 autoplay : true,
//                 autoplayHoverPause : true,
//                 fluidSpeed : 900,
//                 autoplaySpeed : 900,
//                 navSpeed : 200,
//                 dotsSpeed : 100,
//                 dragEndSpeed : 900,
//                 responsive:{ 
//                     0:{
//                         items:1
//                     },
//                     480:{
//                         items:2
//                     }
//                 }
//             });
//         } else{
//             $(".people").removeClass("owl-carousel").trigger('destroy.owl.carousel');
//         }
//     }


//     Chart.defaults.global.defaultFontSize = 18;
//     var dataPie = {
//       labels: [
//         "Лотерея",
//         "Команда",
//         "pre-ICO",
//         "Bounty",
//         "ICO"
//       ],
//       datasets: [{
//         data: [18.65, 12.8, 65, 3, 0.55],
//         backgroundColor: [
//           "#7754f2",
//           "#984ee7",
//           "#c751cd",
//           "#4670f4",
//           "#00baff"
//         ],
//         hoverBackgroundColor: [
//           "#9176f2",
//           "#b487e5",
//           "#d48cd8",
//           "#9bb4ff",
//           "#5bcdf7"
//         ]
//       }]
//     };
//     var pie = $("#piechart");
//     var myPie = new Chart(pie, {
//       type: 'pie',
//       data: dataPie,
//       options: {
//         legend: {
//           display: true,
//           position: 'bottom'        }
//       }
//     });


//     var dataDonut = {
//       labels: [
//         "Heading1",
//         "Heading2",
//         "Heading3"
//       ],
//       datasets: [{
//         data: [70, 20, 10],
//         backgroundColor: [
//           "#984ee7",
//           "#c751cd",
//           "#00baff"
//         ],
//         hoverBackgroundColor: [
//           "#b487e5",
//           "#d48cd8",
//           "#5bcdf7"
//         ]
//       }]
//     };
//     var donut = $("#donutchart");
//     var myDonut = new Chart(donut, {
//       type: 'doughnut',
//       data: dataDonut
//     });







});