<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadSettingsData
 *
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class LoadSettingsData implements FixtureInterface
{
    /**
     * @var array
     */
    protected $data = [
	['paramGroup' => 'landing', 'paramKey' => 'invested_amount', 'value' => '100000', 'enabled' => '1'],
	['paramGroup' => 'smart_contract', 'paramKey' => 'pre_ico_wallet', 'value' => false, 'enabled' => '1'],
	['paramGroup' => 'smart_contract', 'paramKey' => 'ico_wallet', 'value' => false, 'enabled' => '1'],
	['paramGroup' => 'smart_contract', 'paramKey' => 'current_token_price', 'value' => 0.0, 'enabled' => '1'],
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
	foreach($this->data as $parameter){
	    $manager->getRepository('AppBundle:Settings')->addSetting($parameter);
	}
    }
}