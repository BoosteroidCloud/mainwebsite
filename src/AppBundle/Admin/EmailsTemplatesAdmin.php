<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use AppBundle\Entity\EmailsTemplates;
use Sonata\AdminBundle\Route\RouteCollection;

class EmailsTemplatesAdmin extends AbstractAdmin
{
    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
	$collection
		->add('sent_template', 'sent-template', [], [], ['expose' => true])
	;
    }
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('locale')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('locale')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'sent' => [
			    'template' => 'AppBundle:Admin:CRUD/list_action_sent.html.twig'
                    ],
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
                ->with('Template', ['class' => 'col-md-8'])
                    ->add('template', CKEditorType::class, [
		    'config_name' => 'form_template',
		    ])
		    ->end()
                ->with('Describe', ['class' => 'col-md-4'])
                    ->add('locale', 'choice', [
			'choices' => EmailsTemplates::getLocalesArray(),
                    ])
                    ->add('name')
		    ->end()
                
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('template', 'html',[])
            ->add('name')
            ->add('locale')
        ;
    }
}
