<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * Referrals
 */
class Referrals
{
    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $refferalUserId;

    /**
     * @var \AppBundle\Entity\Users
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set refferalUserId
     *
     * @param integer $refferalUserId
     *
     * @return Referrals
     */
    public function setRefferalUserId($refferalUserId)
    {
        $this->refferalUserId = $refferalUserId;

        return $this;
    }

    /**
     * Get refferalUserId
     *
     * @return integer
     */
    public function getRefferalUserId()
    {
        return $this->refferalUserId;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     *
     * @return Referrals
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
