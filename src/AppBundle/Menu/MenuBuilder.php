<?php

namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class MenuBuilder
{
    private $factory;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory)
    {
        $this->factory = $factory;
    }

    public function createMainMenu(RequestStack $requestStack)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('left_menu.token', ['route' => 'token_sale'])
		->setChildrenAttributes(['title' => 'Token'])->setExtra('translation_domain', 'account')
	;
        $menu->addChild('Withdraw', ['route' => 'withdraw'])
		->setChildrenAttributes(['title' => 'Withdraw NFT'])->setAttribute('class', 'hide-elem')
	;
//        $menu->addChild('left_menu.bounty', ['route' => 'bounty'])
//		->setChildrenAttributes(['title' => 'Bounty'])
//	;
        $menu->addChild('left_menu.settings', ['route' => 'profile'])
		->setChildrenAttributes(['title' => 'My account settings'])
	;
        $menu->addChild('left_menu.faq', ['route' => 'faq'])
		->setChildrenAttributes(['title' => 'FAQ'])
	;

        return $menu;
    }
}