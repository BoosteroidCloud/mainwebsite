$(document).ready(function(){
//    $('html, body').animate({
//        scrollTop: $(".starts").offset().top
//    }, 1000);
//not need any more
    $('.language_select li').click(function(){
        var setLang = $('.language_select').data('location'),
        dataLangSelect = $(this).data('lang');
        $('.language_select').data('location', dataLangSelect);
        $('.language_select li').removeClass('active');
        $(this).toggleClass('active');
    });

    $(".toggle_menu").click(function() {
        $(this).toggleClass("on");
        $(".mobail_nav").slideToggle();
    });

    $(window).scroll(function () {
        var top = $(document).scrollTop();
        var height = $(".top_line").height();
        var scroll = $(document).scrollTop();
        var screenHeight = window.innerHeight;
        if (top > height) {
            $(".top_line").addClass("fixed");
        } else {
            $(".top_line").removeClass("fixed");
        }
    });

/*  machine learning  */
    var $root = $('.machine_learning');
    var $form = $root.find('form');
    var $source = $root.find('.source img');
    var $upload = $form.find('.upload input');
    var $output = $root.find('.output figure img');
    var $reference = $root.find('.references input:radio');
    var $mlbutton = $('.machine_learning_button');

//    $mlbutton.on('click', function (event) {
//        $('.machine_learning_input').click();
//    });


    function openFilepicker() {
        $root.find('.upload input').trigger('click');
    }

    function selectPicture(event) {
        var selectedFile = $upload.get(0).files[0];
        var reader = new FileReader();
        var size = $upload.get(0).files[0].size;

        if (size > 2000000) {
            $upload.val('');
            alert('Image size must be less then 2 MB');
        } else {
            reader.onload = function (event) {
                $source.attr('src', event.target.result);
                $output.attr('src', event.target.result);
            };

            reader.readAsDataURL(selectedFile);
            $form.trigger('submit');
        }
    }

    function selectReference(event) {
        if ($upload.val()) {
            $form.trigger('submit');
        } else {
            defaultImageset($(this).val());
        }
    }

  function process(event) {
    event.preventDefault();

    var formData = new FormData($form.get(0));

    $output.closest('.holder').addClass('processing');
    $upload.prop('disabled', true);
    $reference.prop('disabled', true);

    var xhr = $.ajax({ 
      url: 'https://boosteroid.heyml.com/send',
      method: 'POST',
      data: formData, 
      contentType: false,
      processData: false
    });

    xhr.done(function(data) {
      $output.one('load', fadeIn);
      $output.filter('.aws').attr('src', data.links[0]);
      $output.filter('.google').attr('src', data.links[1]);
      $output.filter('.boosteroid').attr('src', data.links[2]);
    });

     xhr.fail(function(data) {
       defaultImageset(0);
     });

    xhr.always(function(data) {
      $upload.prop('disabled', false);
      $reference.prop('disabled', false);
    });
  }

  function defaultImageset(styleId) {
    $output.filter('.aws').attr('src', '/landing/img/aws_' + styleId + '.jpg');
    $output.filter('.google').attr('src', '/landing/img/google_' + styleId + '.jpg');
    $output.filter('.boosteroid').attr('src', '/landing/img/boosteroid_' + styleId + '.jpg');
  }
    
  function fadeIn() {
    $(this).closest('.holder').removeClass('processing');
  };

  $upload.on('change', selectPicture);
  $reference.on('change', selectReference);
  $form.on('submit', process);

/*  machine learning  */


var ctx = document.getElementById('chart').getContext("2d");

var gradientStrokeMain = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeMain.addColorStop(1, '#00c6ff');
gradientStrokeMain.addColorStop(0, '#5c36ee');

var gradientStrokeGoogle = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeGoogle.addColorStop(1, '#1c374f'); 

gradientStrokeGoogle.addColorStop(0, '#fcfdfd');

var gradientStrokeAWS = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeAWS.addColorStop(1, '#fff7bc');
gradientStrokeAWS.addColorStop(0, '#fff7bc');

var gradientStrokeAlternate = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeAlternate.addColorStop(1, '#8c77ff');
gradientStrokeAlternate.addColorStop(0, '#8c77ff');

var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["", "09.17", "02.18", "05.18", "08.18"],
        datasets: [{
            label: "Boosteroid",
            lineTension: 0,
            borderColor: gradientStrokeMain,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeMain,
            // pointHoverBorderColor: gradientStrokeMain,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 6,
            data: [0.1, 0.48, 2.78, 4, 6]
        },
        {
            label: "AWS",
            borderColor: gradientStrokeAWS,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",                       
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [10.6, 10.55, 10.5, 10.45, 10.4]
        },
        {
            label: "Microsoft",
            lineTension: 0,
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [10.26, 10.2, 10.15, 10.1, 10.05]
        },
        {
            label: "Google cloud",
            lineTension: 0,
            borderColor: gradientStrokeGoogle ,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [6.5, 6.45, 6.4, 6.35, 6.3]
        }
      ]
    },
    options: {
        maintainAspectRatio: false,
        animation: {
          easing: "easeInOutBack"
        },
        legend: {
            position: "bottom"
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    // maxTicksLimit: 5,
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                },
                gridLines: {
                    drawTicks: true,
                    display: true,
                    zeroLineColor: "#223b50",
                    color: "#0e2b44"
                }

            }],
            xAxes: [{
                gridLines: {
                    zeroLineColor: "transparent",
                    color: "#0e2b44"
                },
                ticks: {
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                }
            }]
        }
    }
});

   //var nice = $("html").niceScroll({zindex: 10002, scrollspeed:120, mousescrollstep:16, cursorcolor:'#061327 ', cursorborder:'0px solid #ffffff ', cursoropacitymax:1, cursorwidth:4, cursorborderradius:0, horizrailenabled:false});

    window.setInterval(countDown, 500);

    function countDown() {
        var now = new Date();
        var future = new Date("2/15/2018 21:00:00 GMT+1");
        var timeLeft = future - now;
        var milli = timeLeft;

        var seconds = milli / 1000;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var spareSeconds = seconds % 60;
        var spareMinutes = minutes % 60;
        var spareHours = hours % 24;
        var spareDays = days % 365;

        minutes = parseInt(minutes);
        hours = parseInt(hours);
        days = parseInt(days);
        spareSeconds = parseInt(spareSeconds);
        spareMinutes = parseInt(spareMinutes);
        spareHours = parseInt(spareHours);
        spareDays = parseInt(spareDays);

        var mySpan = document.getElementById("timer");
        var myseconds = document.querySelector('.seconds').innerHTML = spareSeconds;
        var myminutes = document.querySelector('.minutes').innerHTML = spareMinutes;
        var myhours = document.querySelector('.hours').innerHTML = spareHours;
        var mydays = document.querySelector('.days').innerHTML = spareDays;
        var myseconds_f = document.querySelector('.seconds_f').innerHTML = spareSeconds;
        var myminutes_f = document.querySelector('.minutes_f').innerHTML = spareMinutes;
        var myhours_f = document.querySelector('.hours_f').innerHTML = spareHours;
        var mydays_f = document.querySelector('.days_f').innerHTML = spareDays;

        var timer_f = $('#timer_f');

        if (milli <= 0) { //Time's run out! If all values go to zero
            var timer_f = $('#timer_f');
          mySpan.innerHTML = "00:00:00";
          timer_f.innerHTML = "00:00:00";
        }
    }

setTimeout(function(){ $('h1').css({ 'display': 'block'})}, 7000);
var video = $("video");
video.bind('click', function() {
            if (video.get(0).paused) {
                video.get(0).play();
        } else {
                video.get(0).pause();
        }
});

    $('#faq li > .item').click(function(){
        $(this).toggleClass('active');
        $(this).next('div').slideToggle(200);
    });


    $("header nav").on("click","a[href^='#']", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
            top = $(id).offset().top;  
            $('body,html').animate({scrollTop: top}, 1500);
    });

    $("header .mobail_nav").on("click","a[href^='#']", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
            top = $(id).offset().top; 
            $(".mobail_nav").slideToggle();
            $(".toggle_menu").toggleClass("on");
            $('body,html').animate({scrollTop: top}, 1500);
    });


    $(".footer_nav ul").on("click","a[href^='#']", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;  
        $('body,html').animate({scrollTop: top}, 1500);
    });


$(".companies.owl-carousel").owlCarousel({
    items : 5,
    nav : true,
    navText : " ",
    loop : true,
    margin: 10,
    autoplay : true,
    autoplayHoverPause : true,
    fluidSpeed : 900,
    autoplaySpeed : 900,
    navSpeed : 200,
    dotsSpeed : 100,
    dragEndSpeed : 900,
    responsive:{ 
        0:{
            items:1
        },
        480:{
            items:2
        },
        768:{
            items:3
        },
        992:{
            items:5
        }
    }
});

$(".people.owl-carousel").owlCarousel({
    items : 5,
    nav : true,
    navText : " ",
    loop : true,
    margin: 10,
    autoplay : true,
    autoplayHoverPause : true,
    fluidSpeed : 900,
    autoplaySpeed : 900,
    navSpeed : 200,
    dotsSpeed : 100,
    dragEndSpeed : 900,
    responsive:{ 
        0:{
            items:1
        },
        480:{
            items:2
        },
        768:{
            items:3
        },
        992:{
            items:5
        }
    }
});

// Get the modal
//        var modalBaunty = document.getElementById('bautny_modal');
//        var modal_button = document.getElementById('modal_baunty');
//        // Get the <span> element that closes the modal
//        var spanClose = document.getElementsByClassName("close_baunty")[0];
//            
//        // When the user clicks on <span> (x), close the modal
//        spanClose.onclick = function() {
//            modalBaunty.style.display = "none";
//        };
//         
//        modal_button.onclick = function() {
//            modalBaunty.style.display = "flex";
//            return false;
//        };
//        // When the user clicks anywhere outside of the modal, close it
//        window.onclick = function(event) {
//            if (event.target == modalBaunty) {
//                modalBaunty.style.display = "none";
//            }
//        };
});

