<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * IcoStages
 */
class IcoStages
{
    const ICO_STAGES_FIRST = '1-stage';
    const ICO_STAGES_SECOND = '2-stage';
    const ICO_STAGES_THIRD = '3-stage';

    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $stage;

    /**
     * @var integer
     */
    private $totalTokens;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $icoRounds;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->icoRounds = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stage
     *
     * @param string $stage
     *
     * @return IcoStages
     */
    public function setStage($stage)
    {
        $this->stage = $stage;

        return $this;
    }

    /**
     * Get stage
     *
     * @return string
     */
    public function getStage()
    {
        return $this->stage;
    }

    /**
     * Set totalTokens
     *
     * @param integer $totalTokens
     *
     * @return IcoStages
     */
    public function setTotalTokens($totalTokens)
    {
        $this->totalTokens = $totalTokens;

        return $this;
    }

    /**
     * Get totalTokens
     *
     * @return integer
     */
    public function getTotalTokens()
    {
        return $this->totalTokens;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return IcoStages
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return IcoStages
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Add icoRound
     *
     * @param \AppBundle\Entity\IcoRounds $icoRound
     *
     * @return IcoStages
     */
    public function addIcoRound(\AppBundle\Entity\IcoRounds $icoRound)
    {
        $this->icoRounds[] = $icoRound;

        return $this;
    }

    /**
     * Remove icoRound
     *
     * @param \AppBundle\Entity\IcoRounds $icoRound
     */
    public function removeIcoRound(\AppBundle\Entity\IcoRounds $icoRound)
    {
        $this->icoRounds->removeElement($icoRound);
    }

    /**
     * Get icoRounds
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIcoRounds()
    {
        return $this->icoRounds;
    }

    /**
     * @return boolean
     */
    public function isStageFirst()
    {
        return $this->stage === self::ICO_STAGES_FIRST ? true : false;
    }

    /**
     * @return boolean
     */
    public function isStageSecond()
    {
        return $this->stage === self::ICO_STAGES_SECOND ? true : false;
    }

    /**
     * @return boolean
     */
    public function isStageThird()
    {
        return $this->stage === self::ICO_STAGES_THIRD ? true : false;
    }

    /**
     * @return array
     */
    public static function getStagesAsLabels()
    {
        return [
            'admin.ico_stages.stages_1' => self::ICO_STAGES_FIRST,
            'admin.ico_stages.stages_2' => self::ICO_STAGES_SECOND,
            'admin.ico_stages.stages_3' => self::ICO_STAGES_THIRD,
        ];
    }
}
