<?php

namespace AppBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Swift_Mailer;
use AppBundle\Entity\Subscribers;
use AppBundle\Entity\EmailsTemplates;
use Mailgun\Mailgun;
//use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class MailerService 
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /*
     * mailgun client
     * $mgClient
     */
    private $mgClient;
    
    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em)
    {
	$this->container = $container;
	$this->em = $em;
        $this->mgClient = new Mailgun('key-3471fd8b321afec148c4af47b40c9516');
    }

    /**
     * @return string
     */
    protected function getMailerUser()
    {
        return $this->container->getParameter('mailer_user');
    }

    /**
     * @return string
     */
    protected function getMailerFrom()
    {
        return iconv("UTF-8", "ISO-8859-1//IGNORE", $this->container->getParameter('mail_from'));
    }

    /**
     * @return string
     */
    protected function getMailerAdminUser()
    {
        return $this->container->getParameter('mail_admin_user');
    }

    /**
     * @return TwigService
     */
    protected function getTemplating()
    {
        return $this->container->get('templating');
    }

    /**
     * @return \Swift_Mailer
     */
    protected function getSwiftMailer()
    {
        return $this->container->get('mailer');
    }

    /**
     * @param Subscribers $subscriber
     * @return bool
     */
    public function subscribeMail(Subscribers $subscriber)
    {
        $translator = $this->container->get('translator.default');
        $tempalate = $this->em->getRepository('AppBundle:EmailsTemplates')->find(1);
        $mgClient = $this->mgClient;
        $domain = "blog.boosteroid.com";

        $messageBuilder = $mgClient->MessageBuilder();
        $messageBuilder->setFromAddress($this->getMailerFrom());
        $messageBuilder->addToRecipient($subscriber->getEmail());
        $messageBuilder->setSubject($translator->trans('subscribe_newsletters.subject', [], 'mail'));
        $messageBuilder->setHtmlBody(
            $this->getTemplating()->render(
                        'AppBundle:Mail:subscribe.html.twig', ['subscriber' => $subscriber, 'tempalate' => $tempalate]
                )   
        );
        $res = $mgClient->post("{$domain}/messages", $messageBuilder->getMessage());

        if($res->http_response_code=='200'){
          return  true;
        }
        return false;
    }
    
    /**
     * @param Subscribers $subscriber
     * @return bool
     */
    public function subscribeAddToListMail(Subscribers $subscriber)
    {
        $translator = $this->container->get('translator.default');

        $mgClient = $this->mgClient;
        
        $listAddress = 'subscribers@blog.boosteroid.com';

        # Issue the call to the client.
        $result = $mgClient->post("lists/$listAddress/members", array(
            'address'     => $subscriber->getEmail(),
            'name'        => 'landing page',
            'description' => 'landing page',
            'subscribed'  => true
        ));
        if($result->http_response_code=='200'){
          return  true;
        }
        return false;
    }
    
    /**
     * @param Subscribers $subscriber
     * @return bool
     */
    public function sendTemplateToUsersMail(EmailsTemplates $template)
    {
        $translator = $this->container->get('translator.default');
        $mgClient = $this->mgClient;
        $domain = "blog.boosteroid.com";
        $users = $this->em->getRepository('AppBundle:Users')->findByEnabled(1);
        foreach ($users as $value) {
            $messageBuilder = $mgClient->MessageBuilder();
            $messageBuilder->setFromAddress($this->getMailerFrom());
            $messageBuilder->addToRecipient($value->getEmail());
            $messageBuilder->setSubject('Don’t miss the start of the 1st ICO stage. Only two days left');
            $messageBuilder->setHtmlBody(
                $this->getTemplating()->render(
                            'AppBundle:Mail:sent_template.html.twig', ['tempalate' => $template]
                    )   
            );
            $res = $mgClient->post("{$domain}/messages", $messageBuilder->getMessage());
        }
        return true;
    }
}