<?php

namespace AppBundle\Repository;

/**
 * UsersRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class UsersRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * 
     * @param type $dateStart
     * @param type $dateEnd
     */
    public function getUsersFromRangeCreated($dateStart, $dateEnd) 
    {
        $dateStart = \DateTime::createFromFormat('d/m/Y H:i' ,$dateStart.' 00:00');
        $dateEnd = \DateTime::createFromFormat('d/m/Y H:i' ,$dateEnd.' 00:00');
        $qb = $this->createQueryBuilder('user');
        $query = $qb->where('user.createdAt > :start')
        ->andWhere('user.createdAt < :end')
        ->setParameter('start', $dateStart)
        ->setParameter('end', $dateEnd)
        ->getQuery()
        ->getResult()
        ;
        
        return $query;
    }
    
}
