<?php

namespace AppBundle\Services\SmartContract;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;
use AppBundle\Entity\IcoStages;

/**
 * @author Nikc Zhulinskyi <zhulinskyi1990@gmail.com>
 * @author Vladimir Sergeeev <vladimirsserg@gmail.com>
 */
class SmartContractService 
{
    const API_URL = 'https://api.etherscan.io/api';
    const SMART_CONTRACT = '0x57d90b64a1a57749b0f932f1a3395792e12e7055'; //test data
    const ICO_WALLET = '0xe04f27eb70e025b78871a2ad7eabe85e61212761'; //test data

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var string
     */
    private $etherscanApiKey;

    /**
     * Constructor
     */
    public function __construct(ContainerInterface $container, EntityManager $em, $etherscanApiKey)
    {
	$this->container = $container;
	$this->em = $em;
        $this->etherscanApiKey = $etherscanApiKey;
    }

    /**
     * Get Smart Contract address
     * 
     * @return string
     */
    public function getSmartContract()
    {
        return $this->em->getRepository('AppBundle:SmartContracts')->getSmartContractAddress();
    }

    /**
     * Get Ico wallet from settings
     * 
     * @return string
     */
    public function getIcoWallet()
    {
        return $this->em->getRepository('AppBundle:Settings')->getIcoWellet();
    }

    /**
     * Get ERC20-Token Account Balance for TokenContractAddress 
     * 
     * @link https://etherscan.io/apis#tokens
     * @return int
     */
    public function getRemainToken()
    {
        $smartContract = $this->getSmartContract();

        if(empty($this->getSmartContract()) || empty($this->getIcoWallet())){
            return 0;
        }
        // https://api.etherscan.io/api?module=account&action=tokenbalance&contractaddress=0xC01281a1F3f202AfFae40E4DF130E4CC8ED108cB&address=0xbD00cE9C7c88E05373ecabA02a63277e8d9cF3e5&tag=latest&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS'); 
        $dataUrl = 'account&action=tokenbalance&contractaddress='.$smartContract.'&address='.$this->getIcoWallet().'&tag=latest&apikey='.$this->etherscanApiKey;
        $result = $this->createQuery($dataUrl);
        return $result->result !== false ? $result->result : 0;       
    }

    /**
     * Get ERC20-Token TotalSupply by ContractAddress
     * 
     * @link https://etherscan.io/apis#tokens
     * @return string|object
     */
    public function getTotalSupply()
    {
        $smartContract = $this->getSmartContract();

        if(empty($this->getSmartContract())){
            return 0;
        }
        //https://api.etherscan.io/api?module=stats&action=tokensupply&contractaddress=0xC01281a1F3f202AfFae40E4DF130E4CC8ED108cB&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS
        $dataUrl = 'stats&action=tokensupply&contractaddress='.$smartContract.'&apikey='.$this->etherscanApiKey;
        $result = $this->createQuery($dataUrl);
        return $result->result !== false ? $result->result : 0;
    }

    public function getSaleTokens() {
//        https://api.etherscan.io/api?module=account&action=balance&address=0xfFF58E2Bd882dd1D22f8EEBbA2c5B3d7f07A7fe9&tag=latest&apikey=XIWYPDU1A5NMXKR3MRHDT2WZ36QU8B9VCS
	$smartData = file_get_contents('https://api.etherscan.io/api?module=account&action=balance&address=0xfFF58E2Bd882dd1D22f8EEBbA2c5B3d7f07A7fe9&tag=latest&apikey='.$this->etherscanApiKey);
	$result = json_decode($smartData);
        $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->find(1);
        $remainToken = $this->getBalance();
        $result = ($smartContract->getTotalTokens() - $remainToken->result); 

        return $result;
    }

    /**
     * @return bool
     */
    public function setRamainTokens()
    {
        $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->getSmartContractInfo();

        try {
            if (empty($smartContract)) {
                $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->createEmptyContract();
            } else {
                $smartContract
                        ->setTotalTokens($this->getFloatNumberTokens($this->getTotalSupply()))
                        ->setBalance($this->getFloatNumberTokens($this->getRemainToken()))
                ;
            }

            $this->em->persist($smartContract);
            $this->em->flush();
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return true;
    }

    /**
     * @param string $dataUrl
     * @return string
     */
    protected function createQuery($dataUrl)
    {
        $url = self::API_URL . '?module=' . $dataUrl;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        if ($result === false || $result == '') {
            //$result = curl_errno($ch) . ': ' . curl_error($ch);
            return $result;
        }

        curl_close($ch);

        return json_decode($result);
    }

    /**
     * @param float $data
     */
    public function getFloatNumberTokens($data)
    {
        return $data / 100;
    }

    /**
     * @param SmartContracts $smartContract
     * @return array
     */
    public function getRemainderTokensInStages(SmartContracts $smartContract)
    {
        $em = $this->getEntityManager();
        $stages = $em->getRepository('AppBundle:IcoStages')->findBy([], ['stage', 'ASC']);

        $remainderStage = [];
        $stageTokens = 0;

        foreach ($stages as $stage_) {
            $stageTokens += $stage_->getTotalTokens();
            $remainderStage[$stage_->getStage()] = $smartContract->getIcoTokens() - $stageTokens;
        }

        return $remainderStage;
    }

    /**
     * @return IcoRounds
     */
    public function getCurrentIcoRound()
    {
        $smartContract = $this->em->getRepository('AppBundle:SmartContracts')->getSmartContractInfo();
        if (!empty($smartContract)) {
            $rounds = $this->em->getRepository('AppBundle:IcoRounds')->getAllRounds();
            $totalTokens = 0;
            $currentRound = null;

            foreach ($rounds as $round) {
                $remainder = $smartContract->getIcoTokens() - $totalTokens - $round->getTokensAmount();
                if ($smartContract->getBalance() <= $smartContract->getTotalTokens() && $smartContract->getBalance() >= $remainder) {
                    $currentRound = $round;
                    break;
                }
                $totalTokens += $round->getTokensAmount();
            }

            return $currentRound;
        }
        return;
    }

}