<?php

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\Timestampable;

/**
 * BountyProgram
 */
class BountyProgram
{
    use Timestampable;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $link;

    /**
     * @var integer
     */
    private $bonusTokens;

    /**
     * @var \AppBundle\Entity\Users
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     *
     * @return BountyProgram
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set bonusTokens
     *
     * @param integer $bonusTokens
     *
     * @return BountyProgram
     */
    public function setBonusTokens($bonusTokens)
    {
        $this->bonusTokens = $bonusTokens;

        return $this;
    }

    /**
     * Get bonusTokens
     *
     * @return integer
     */
    public function getBonusTokens()
    {
        return $this->bonusTokens;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\Users $user
     *
     * @return BountyProgram
     */
    public function setUser(\AppBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}
