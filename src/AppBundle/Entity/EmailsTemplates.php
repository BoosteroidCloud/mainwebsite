<?php

namespace AppBundle\Entity;

/**
 * EmailsTemplates
 */
class EmailsTemplates
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $locale;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set template
     *
     * @param string $template
     *
     * @return EmailsTemplates
     */
    public function setTemplate($template)
    {
        $this->template = $template;

        return $this;
    }

    /**
     * Get template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return EmailsTemplates
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }
    
    /**
     * @return array
     */
    public static function getLocalesArray()
    {
	return [
	    'en_EN' => 'en_EN',
	    'ja_JP' => 'ja_JP',
	    'ru_RU' => 'ru_RU',
            'zh_CN' => 'zh_CN',
	];
    }
    /**
     * @var string
     */
    private $name;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return EmailsTemplates
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
