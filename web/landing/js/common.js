$(document).ready(function(){

    $(".toggle_menu").click(function() {
        $(this).toggleClass("on");
        $("nav").slideToggle();
    });

  
    window.setInterval(countDown, 500);

    function countDown() {
        var now = new Date();
        var future = new Date("10/30/2017 12:00:00");
        var timeLeft = future - now;
        var milli = timeLeft;

        var seconds = milli / 1000;
        var minutes = seconds / 60;
        var hours = minutes / 60;
        var days = hours / 24;
        var spareSeconds = seconds % 60;
        var spareMinutes = minutes % 60;
        var spareHours = hours % 24;
        var spareDays = days % 365;

        minutes = parseInt(minutes);
        hours = parseInt(hours);
        days = parseInt(days);
        spareSeconds = parseInt(spareSeconds);
        spareMinutes = parseInt(spareMinutes);
        spareHours = parseInt(spareHours);
        spareDays = parseInt(spareDays);

        var mySpan = document.getElementById("timer");
        var myseconds = document.querySelector('.seconds').innerHTML = spareSeconds;
        var myminutes = document.querySelector('.minutes').innerHTML = spareMinutes;
        var myhours = document.querySelector('.hours').innerHTML = spareHours;
        var mydays = document.querySelector('.days').innerHTML = spareDays;
        var myseconds_f = document.querySelector('.seconds_f').innerHTML = spareSeconds;
        var myminutes_f = document.querySelector('.minutes_f').innerHTML = spareMinutes;
        var myhours_f = document.querySelector('.hours_f').innerHTML = spareHours;
        var mydays_f = document.querySelector('.days_f').innerHTML = spareDays;

        var timer_f = $('#timer_f');

        if (milli <= 0) { //Time's run out! If all values go to zero
            var timer_f = $('#timer_f');
          mySpan.innerHTML = "00:00:00";
          timer_f.innerHTML = "00:00:00";
        }
    }

setTimeout(() => $('h1').css({ 'display': 'block'}), 7000);




// $(window).scroll(function() {
//         var top = $(document).scrollTop();

//         $('.slogan p').animate({'margin-top':'9000px', opacity:0}, 3000);
//         $('.main').animate({'opacity':'1'}, 3000);
//         $('.main').css({ 'display':' block'}, 3000);
//         // $('h1').animate({ 'width':' 100%'}, 3000);
//         $('h1').css({ 'display':' block'}, 3000);
//         $('body').css({ 'top':' 0', 'position':'reletive'}, 3000);
//         return false;
//     });



var ctx = document.getElementById('chart').getContext("2d");

var gradientStrokeMain = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeMain.addColorStop(1, '#00c6ff');
gradientStrokeMain.addColorStop(0, '#5c36ee');

var gradientStrokeAlternate = ctx.createLinearGradient(500, 0, 100, 0);
gradientStrokeAlternate.addColorStop(1, '#1c374f');
gradientStrokeAlternate.addColorStop(0, '#fcfdfd');

var chart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["", "5.09", "19.09", "2018", "2019"],
        datasets: [{
            label: "Boosteroid",
            lineTension: 0,
            borderColor: gradientStrokeMain,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeMain,
            // pointHoverBorderColor: gradientStrokeMain,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 6,
            data: [0.5, 0.5, 2.5, 5.5, 6.5]
        },
        {
            label: "AWS",
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",                       
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [12, 12, 12, 12.5, 13]
        },
        {
            label: "Microsoft",
            lineTension: 0,
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [8.5, 9, 9, 9, 10]
        },
        {
            label: "Flyelephant",
            lineTension: 0,
            borderColor: gradientStrokeAlternate,
            pointBorderColor: "rgba(81, 200, 255, 0.3)",
            pointBackgroundColor: "#fff",
            // pointHoverBackgroundColor: gradientStrokeAlternate,
            // pointHoverBorderColor: gradientStrokeAlternate,
            pointBorderWidth: 6,
            pointHoverRadius: 3,
            pointHoverBorderWidth: 4,
            pointRadius: 2,
            fill: true,
            backgroundColor: "transparent",
            borderWidth: 2,
            data: [5.5, 6.5, 7.5, 7.5, 8.5]
        }
      ]
    },
    options: {
        animation: {
          easing: "easeInOutBack"
        },
        legend: {
            position: "bottom"
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true,
                    // maxTicksLimit: 5,
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                },
                gridLines: {
                    drawTicks: true,
                    display: true,
                    zeroLineColor: "#223b50",
                    color: "#0e2b44"
                }

            }],
            xAxes: [{
                gridLines: {
                    zeroLineColor: "transparent",
                    color: "#0e2b44"
                },
                ticks: {
                    padding: 20,
                    fontFamily: "arial, sans-serif",
                    fontWeight: 100,
                    fontColor: "#3f759f",
                    fontSize: 14
                }
            }]
        }
    }
});

    $('#faq li > .item').click(function(){
        $(this).toggleClass('active');
        $(this).next('div').slideToggle(200);
    });


    $("header nav").on("click","a[href^='#']", function (event) {
            event.preventDefault();
            var id  = $(this).attr('href'),
            top = $(id).offset().top;  
            $('body,html').animate({scrollTop: top}, 1500);
    });

    $(".footer_nav ul").on("click","a[href^='#']", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;  
        $('body,html').animate({scrollTop: top}, 1500);
    });




// var DrawEye = function(eyecontainer, pupil, eyeposx, eyeposy){
//   // Initialise core variables
//   var r = $(pupil).width()/2;
//   var center = {
//     x: $(eyecontainer).width()/2 - r, 
//     y: $(eyecontainer).height()/2 - r
//   };
//   var distanceThreshold = $(eyecontainer).width()/1.5 - r;
//   var mouseX = 0, mouseY = 0;
  
//   // Listen for mouse movement
//   $(window).mousemove(function(e){ 
//     var d = {
//       x: e.pageX - r - eyeposx - center.x,
//       y: e.pageY - r - eyeposy - center.y
//     };
//     var distance = Math.sqrt(d.x*d.x + d.y*d.y);
//     if (distance < distanceThreshold) {
//       mouseX = e.pageX - eyeposx - r;
//       mouseY = e.pageY - eyeposy - r;
//     } else {
//       mouseX = d.x / distance * distanceThreshold + center.x;
//       mouseY = d.y / distance * distanceThreshold + center.y;
//     }
//   });
  
//   // Update pupil location
//   var pupil = $(pupil);
//   var xp = 0, yp = 0;
//   var loop = setInterval(function(){
//     // change 1 to alter damping/momentum - higher is slower
//     xp += (mouseX - xp) / 2;
//     yp += (mouseY - yp) / 2;
//     pupil.css({left:xp, top:yp});    
//   }, 1);
// };


// var eyeleft = new DrawEye(".asteroid_wrap", ".asteroid", 0, 0);


$(".slider_phone.owl-carousel").owlCarousel({
    items : 1,
    nav : true,
    navText : " ",
    loop : true,
    margin: 0,
    autoplay : true,
    autoplayHoverPause : true,
    fluidSpeed : 900,
    autoplaySpeed : 900,
    navSpeed : 200,
    dotsSpeed : 100,
    dragEndSpeed : 900
});

// if(window.innerWidth <= 992){
//     $(".people").addClass("owl-carousel");
// } else{
//     $(".people").removeClass("owl-carousel");
// }


$(".people.owl-carousel").owlCarousel({
    items : 5,
    nav : true,
    navText : " ",
    loop : true,
    margin: 10,
    autoplay : true,
    autoplayHoverPause : true,
    fluidSpeed : 900,
    autoplaySpeed : 900,
    navSpeed : 200,
    dotsSpeed : 100,
    dragEndSpeed : 900,
    responsive:{ 
        0:{
            items:1
        },
        480:{
            items:2
        },
        768:{
            items:3
        },
        992:{
            items:5
        }
    }
});

window.onresize = function(){
    if(window.innerWidth <= 992){
        $(".people").addClass("owl-carousel").owlCarousel({
            items : 3,
            nav : true,
            navText : " ",
            loop : true,
            margin: 10,
            autoplay : true,
            autoplayHoverPause : true,
            fluidSpeed : 900,
            autoplaySpeed : 900,
            navSpeed : 200,
            dotsSpeed : 100,
            dragEndSpeed : 900,
            responsive:{ 
                0:{
                    items:1
                },
                480:{
                    items:2
                },
                768:{
                    items:3
                }
            }
        });
    } else{
        $(".people").removeClass("owl-carousel").trigger('destroy.owl.carousel');
    }
}




    $(".subscribe_email").submit(function() {
        var form_data = $(this).serialize();
        $.ajax({
            type: "POST",
            url: "mail.php",
            data: form_data,
            success: function() {
                $('.subscribe_email').html("<br/>Thank you. The application was accepted.");
            }
        });
    });



});